package it.com.atlassian.indexer.application;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.atlassian.indexer.application.configuration.IndexerConfiguration;
import com.atlassian.indexer.model.ImmutableRestIndexConfiguration;
import com.atlassian.indexer.model.index.Index;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.jersey.jackson.JacksonMessageBodyProvider;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.glassfish.jersey.client.ClientConfig;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.ClientBuilder;
import java.net.URL;
import java.util.Collection;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public final class VersionTest {
    @ClassRule
    public static final DropwizardAppRule<IndexerConfiguration> SERVICE = AllTests.SERVICE;

    private static final String INDEX_NAME = "version_comments";

    private static final String TABLE_NAME = "version_comment";
    private static final String THREAD_UUID_ATTRIBUTE = "threadUuid";
    private static final String COMMENT_UUID_ATTRIBUTE = "commentUuid";
    private static final String COMMENT_ATTRIBUTE = "comment";
    private static final String VERSION_ATTRIBUTE = "version";

    private static final URL INDEX_FILE = Resources.getResource("comments_index.json");

    private static final ObjectMapper JSON = Jackson.newObjectMapper();

    private static DynamoDBHelper dynamoDBHelper;
    private static IndexerHelper indexerHelper;

    @BeforeClass
    public static void setupClass() {
        dynamoDBHelper = SERVICE.getConfiguration()
                .getDynamoDbEndpoint()
                .map(DynamoDBHelper::new)
                .orElseGet(DynamoDBHelper::new);
        final javax.ws.rs.client.Client client = ClientBuilder.newClient(new ClientConfig()
                .register(new JacksonMessageBodyProvider(JSON)));
        indexerHelper = new IndexerHelper(client, SERVICE.getLocalPort());
    }

    @Test
    public void testExternalVersioning() throws Exception {
        final DynamoDBHelper.TestTable table = dynamoDBHelper.createTable(TABLE_NAME, THREAD_UUID_ATTRIBUTE, COMMENT_UUID_ATTRIBUTE);

        final String threadUuid = UUID.randomUUID().toString();
        final String commentUuid1 = UUID.randomUUID().toString();
        final String commentUuid2 = UUID.randomUUID().toString();
        table.putItem(item(threadUuid, commentUuid1, "hello", 1));
        table.putItem(item(threadUuid, commentUuid2, "world", 1));

        final IndexerHelper.TestIndex index = indexerHelper.createIndex(ImmutableRestIndexConfiguration.builder()
                .withTableName(TABLE_NAME)
                .withStreamCheckpointTableName("version-comment-stream-checkpoint")
                .withIndexName(INDEX_NAME)
                .withIndex(JSON.readValue(INDEX_FILE, Index.class))
                .withVersionAttribute(VERSION_ATTRIBUTE)
                .withVersionType("external")
                .build());

        assertCommentUuidsByThreadUuid(index, "Initial state of ElasticSearch for thread", threadUuid, "hello", "world");

        table.putItem(item(threadUuid, commentUuid1, "hi", 0));
        table.putItem(item(threadUuid, commentUuid2, "universe", 2));

        assertCommentUuidsByThreadUuid(index,
                String.format("Final state of ElasticSearch for thread should include comment %s version 1 and comment %s version 2",
                        commentUuid1, commentUuid2),
                threadUuid,
                "hello", "universe");
    }

    private void assertCommentUuidsByThreadUuid(
            final IndexerHelper.TestIndex index,
            final String expectationDescription,
            final String threadUuid,
            final String... expectedComments) throws InterruptedException {
        final Set<String> expectedValues = Stream.of(expectedComments).collect(Collectors.toSet());

        final Collection<String> foundComments = index.waitFor(THREAD_UUID_ATTRIBUTE, threadUuid, COMMENT_ATTRIBUTE, expectedValues);

        assertThat(foundComments)
                .as(expectationDescription)
                .containsOnly(expectedComments);
    }

    private static Item item(final String threadUuid, final String commentUuid, final String comment, final int version) {
        return new Item()
                .withString(THREAD_UUID_ATTRIBUTE, threadUuid)
                .withString(COMMENT_UUID_ATTRIBUTE, commentUuid)
                .withString(COMMENT_ATTRIBUTE, comment)
                .withInt(VERSION_ATTRIBUTE, version);
    }
}
