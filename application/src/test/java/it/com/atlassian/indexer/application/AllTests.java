package it.com.atlassian.indexer.application;

import com.atlassian.indexer.application.IndexerApplication;
import com.atlassian.indexer.application.configuration.IndexerConfiguration;
import com.googlecode.junittoolbox.SuiteClasses;
import com.googlecode.junittoolbox.WildcardPatternSuite;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.ClassRule;
import org.junit.runner.RunWith;

@RunWith(WildcardPatternSuite.class)
@SuiteClasses("**/*Test.class")
public final class AllTests {
    private AllTests() {
    }

    @ClassRule
    public static final DropwizardAppRule<IndexerConfiguration> SERVICE = new DropwizardAppRule<>(
            IndexerApplication.class, ClassLoader.getSystemResource("test.yml").toString());
}
