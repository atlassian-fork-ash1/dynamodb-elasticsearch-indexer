package it.com.atlassian.indexer.application;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.atlassian.indexer.application.configuration.IndexerConfiguration;
import com.atlassian.indexer.model.ImmutableRestIndexConfiguration;
import com.atlassian.indexer.model.index.Index;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.jersey.jackson.JacksonMessageBodyProvider;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.glassfish.jersey.client.ClientConfig;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.ClientBuilder;
import java.net.URL;
import java.util.Collection;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public final class ApplicationTest {
    @ClassRule
    public static final DropwizardAppRule<IndexerConfiguration> SERVICE = AllTests.SERVICE;

    private static final String INDEX_NAME = "comments";

    private static final String TABLE_NAME = "comment";
    private static final String THREAD_UUID_ATTRIBUTE = "threadUuid";
    private static final String COMMENT_UUID_ATTRIBUTE = "commentUuid";

    private static final URL INDEX_FILE = Resources.getResource("comments_index.json");

    private static final ObjectMapper JSON = Jackson.newObjectMapper();

    private static DynamoDBHelper dynamoDBHelper;
    private static IndexerHelper indexerHelper;

    @BeforeClass
    public static void setupClass() {
        dynamoDBHelper = SERVICE.getConfiguration()
                .getDynamoDbEndpoint()
                .map(DynamoDBHelper::new)
                .orElseGet(DynamoDBHelper::new);
        final javax.ws.rs.client.Client client = ClientBuilder.newClient(new ClientConfig()
                .register(new JacksonMessageBodyProvider(JSON)));
        indexerHelper = new IndexerHelper(client, SERVICE.getLocalPort());
    }

    @Test
    public void testApplication() throws Exception {
        final DynamoDBHelper.TestTable table = dynamoDBHelper.createTable(TABLE_NAME, THREAD_UUID_ATTRIBUTE, COMMENT_UUID_ATTRIBUTE);

        final String threadUuid1 = UUID.randomUUID().toString();
        final String commentUuid1 = UUID.randomUUID().toString();
        final String commentUuid2 = UUID.randomUUID().toString();
        table.putItem(item(threadUuid1, commentUuid1));
        table.putItem(item(threadUuid1, commentUuid2));

        final String threadUuid2 = UUID.randomUUID().toString();
        final String commentUuid3 = UUID.randomUUID().toString();
        table.putItem(item(threadUuid2, commentUuid3));

        final IndexerHelper.TestIndex index = indexerHelper.createIndex(ImmutableRestIndexConfiguration.builder()
                .withTableName(TABLE_NAME)
                .withStreamCheckpointTableName("comment-stream-checkpoint")
                .withIndexName(INDEX_NAME)
                .withIndex(JSON.readValue(INDEX_FILE, Index.class))
                .build());

        assertCommentUuidsByThreadUuid(index, "Initial state of ElasticSearch for thread 1", threadUuid1, commentUuid1, commentUuid2);
        assertCommentUuidsByThreadUuid(index, "Initial state of ElasticSearch for thread 2", threadUuid2, commentUuid3);

        final String newCommentUuid = UUID.randomUUID().toString();
        table.putItem(item(threadUuid1, newCommentUuid));
        table.deleteItem(threadUuid1, commentUuid1);

        assertCommentUuidsByThreadUuid(index,
                String.format("Final state of ElasticSearch for thread 1 should include new comment %s but not deleted comment %s",
                        newCommentUuid, commentUuid1),
                threadUuid1,
                commentUuid2, newCommentUuid);
    }

    private void assertCommentUuidsByThreadUuid(
            final IndexerHelper.TestIndex index,
            final String expectationDescription,
            final String threadUuid,
            final String... expectedCommentUuids) throws InterruptedException {
        final Set<String> expectedValues = Stream.of(expectedCommentUuids).collect(Collectors.toSet());

        final Collection<String> foundUuids = index.waitFor(THREAD_UUID_ATTRIBUTE, threadUuid, COMMENT_UUID_ATTRIBUTE, expectedValues);

        assertThat(foundUuids)
                .as(expectationDescription)
                .containsOnly(expectedCommentUuids);
    }

    private static Item item(final String threadUuid, final String commentUuid) {
        return new Item()
                .withString(THREAD_UUID_ATTRIBUTE, threadUuid)
                .withString(COMMENT_UUID_ATTRIBUTE, commentUuid);
    }
}
