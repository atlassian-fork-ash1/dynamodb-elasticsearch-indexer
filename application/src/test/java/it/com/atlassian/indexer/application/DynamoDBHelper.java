package it.com.atlassian.indexer.application;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.amazonaws.services.dynamodbv2.model.StreamSpecification;
import com.amazonaws.services.dynamodbv2.model.StreamViewType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.amazonaws.regions.Regions.DEFAULT_REGION;

public final class DynamoDBHelper {
    private static final Logger logger = LoggerFactory.getLogger(DynamoDBHelper.class);

    private static final long READ_CAPACITY_UNITS = 5;
    private static final long WRITE_CAPACITY_UNITS = 5;

    private final DynamoDB dynamoDB;

    public DynamoDBHelper() {
        this.dynamoDB = new DynamoDB(AmazonDynamoDBClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials("key", "secret")))
                .build());
    }

    public DynamoDBHelper(final String dynamoDBEndpoint) {
        this.dynamoDB = new DynamoDB(AmazonDynamoDBClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials("key", "secret")))
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(dynamoDBEndpoint, DEFAULT_REGION.getName()))
                .build());
    }

    public TestTable createTable(final String tableName, final String hashKeyName, final String rangeKeyName) {
        try {
            final Table table = dynamoDB.getTable(tableName);

            table.describe();

            logger.debug("Table {} already exists.", tableName);

            return new TestTable(table, hashKeyName, rangeKeyName);
        } catch (final ResourceNotFoundException tableNotFound) {
            logger.debug("Creating table {}.", tableName);

            final Table table = dynamoDB.createTable(
                    new CreateTableRequest()
                            .withTableName(tableName)
                            .withAttributeDefinitions(
                                    new AttributeDefinition().withAttributeName(hashKeyName).withAttributeType("S"),
                                    new AttributeDefinition().withAttributeName(rangeKeyName).withAttributeType("S"))
                            .withKeySchema(
                                    new KeySchemaElement().withAttributeName(hashKeyName).withKeyType(KeyType.HASH),
                                    new KeySchemaElement().withAttributeName(rangeKeyName).withKeyType(KeyType.RANGE))
                            .withProvisionedThroughput(
                                    new ProvisionedThroughput().withReadCapacityUnits(READ_CAPACITY_UNITS).withWriteCapacityUnits(WRITE_CAPACITY_UNITS))
                            .withStreamSpecification(
                                    new StreamSpecification().withStreamEnabled(true).withStreamViewType(StreamViewType.NEW_AND_OLD_IMAGES)));

            return new TestTable(table, hashKeyName, rangeKeyName);
        }
    }

    public class TestTable {
        private final Table table;
        private final String hashKeyName;
        private final String rangeKeyName;

        TestTable(final Table table, final String hashKeyName, final String rangeKeyName) {
            this.table = table;
            this.hashKeyName = hashKeyName;
            this.rangeKeyName = rangeKeyName;
        }

        public void putItem(final Item item) {
            table.putItem(item);
        }

        public void deleteItem(final String hash, final String range) {
            table.deleteItem(hashKeyName, hash, rangeKeyName, range);
        }
    }
}

