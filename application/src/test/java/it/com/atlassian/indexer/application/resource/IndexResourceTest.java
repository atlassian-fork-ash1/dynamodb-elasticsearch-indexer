package it.com.atlassian.indexer.application.resource;

import com.atlassian.elasticsearch.client.Client;
import com.atlassian.elasticsearch.client.gson.GsonParser;
import com.atlassian.elasticsearch.client.gson.GsonRenderer;
import com.atlassian.elasticsearch.client.indices.CreateIndexResponse;
import com.atlassian.elasticsearch.client.okhttp.OkHttpRequestExecutor;
import com.atlassian.indexer.application.configuration.IndexerConfiguration;
import com.atlassian.indexer.model.ImmutableRestIndexConfiguration;
import com.atlassian.indexer.model.RestIndexConfiguration;
import com.atlassian.indexer.model.index.Index;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.jersey.jackson.JacksonMessageBodyProvider;
import io.dropwizard.testing.junit.DropwizardAppRule;
import it.com.atlassian.indexer.application.AllTests;
import it.com.atlassian.indexer.application.DynamoDBHelper;
import okhttp3.OkHttpClient;
import org.glassfish.jersey.client.ClientConfig;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URL;

import static com.atlassian.elasticsearch.client.ES.createIndexSource;
import static com.atlassian.elasticsearch.client.ES.index;
import static org.assertj.core.api.Assertions.assertThat;

public class IndexResourceTest {
    @ClassRule
    public static final DropwizardAppRule<IndexerConfiguration> SERVICE = AllTests.SERVICE;

    private static final String ELASTIC_SEARCH_CLIENT_URL = "http://localhost:9200";

    private static final URL INDEX_FILE = Resources.getResource("comments_index.json");

    private static final String INDEX_NAME = "comments";

    private static final String TABLE_NAME = "comment";
    private static final String THREAD_UUID_ATTRIBUTE = "threadUuid";
    private static final String COMMENT_UUID_ATTRIBUTE = "commentUuid";

    private static final ObjectMapper JSON = Jackson.newObjectMapper();

    private static javax.ws.rs.client.Client client;
    private static Client elasticSearchClient;

    @BeforeClass
    public static void setupClass() {
        client = ClientBuilder.newClient(new ClientConfig()
                .register(new JacksonMessageBodyProvider(JSON)));

        elasticSearchClient = Client.builder()
                .serverUrl(ELASTIC_SEARCH_CLIENT_URL)
                .jsonRenderer(new GsonRenderer())
                .jsonParser(new GsonParser())
                .requestExecutor(OkHttpRequestExecutor.withCallFactory(new OkHttpClient.Builder().build()).build())
                .build();

        SERVICE.getConfiguration()
                .getDynamoDbEndpoint()
                .map(DynamoDBHelper::new)
                .orElseGet(DynamoDBHelper::new)
                .createTable(TABLE_NAME, THREAD_UUID_ATTRIBUTE, COMMENT_UUID_ATTRIBUTE);
    }

    @Test
    public void testCreateIndex() throws IOException {
        final RestIndexConfiguration indexConfiguration = ImmutableRestIndexConfiguration.builder()
                .withTableName(TABLE_NAME)
                .withStreamCheckpointTableName("comment-stream-checkpoint")
                .withIndexName(INDEX_NAME)
                .withIndex(JSON.readValue(INDEX_FILE, Index.class))
                .build();

        final Response response = createIndex(indexConfiguration);

        assertThat(response.getStatus()).isEqualTo(Response.Status.ACCEPTED.getStatusCode());
    }

    @Test
    public void testDeleteIndex() throws InterruptedException {
        final String indexName = "fred";

        final CreateIndexResponse createIndexResponse = createIndex(indexName);

        assertThat(createIndexResponse.isStatusSuccess()).isTrue();

        final Response response = deleteIndex(indexName);

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    public void testDeleteIndexNotFound() throws InterruptedException {
        final String indexName = "bob";

        final Response response = deleteIndex(indexName);

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    public void testDeleteActiveIndex() throws IOException {
        final RestIndexConfiguration indexConfiguration = ImmutableRestIndexConfiguration.builder()
                .withTableName(TABLE_NAME)
                .withStreamCheckpointTableName("comment-stream-checkpoint")
                .withIndexName(INDEX_NAME)
                .withIndex(JSON.readValue(INDEX_FILE, Index.class))
                .build();

        final Response createResponse = createIndex(indexConfiguration);

        assertThat(createResponse.getStatus()).isEqualTo(Response.Status.ACCEPTED.getStatusCode());

        final Response deleteResponse = deleteIndex(INDEX_NAME);

        assertThat(deleteResponse.getStatus()).isEqualTo(Response.Status.FORBIDDEN.getStatusCode());
    }

    private Response createIndex(final RestIndexConfiguration indexConfiguration) {
        return client.target(String.format("http://localhost:%d/rest/indexes", SERVICE.getLocalPort())).request().post(Entity.json(indexConfiguration));
    }

    private CreateIndexResponse createIndex(final String indexName) {
        return elasticSearchClient.execute(index(indexName).create().source(createIndexSource())).join();
    }

    private Response deleteIndex(final String indexName) {
        return client.target(String.format("http://localhost:%d/rest/indexes/%s", SERVICE.getLocalPort(), indexName)).request().delete();
    }
}
