package it.com.atlassian.indexer.application;

import com.atlassian.elasticsearch.client.Client;
import com.atlassian.elasticsearch.client.gson.GsonParser;
import com.atlassian.elasticsearch.client.gson.GsonRenderer;
import com.atlassian.elasticsearch.client.okhttp.OkHttpRequestExecutor;
import com.atlassian.elasticsearch.client.search.Hit;
import com.atlassian.elasticsearch.client.search.SearchRequestBuilder;
import com.atlassian.elasticsearch.client.search.SearchResponse;
import com.atlassian.indexer.model.RestIndexConfiguration;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static com.atlassian.elasticsearch.client.ES.boolQuery;
import static com.atlassian.elasticsearch.client.ES.index;
import static com.atlassian.elasticsearch.client.ES.matchPhraseQuery;
import static com.atlassian.elasticsearch.client.ES.searchSource;
import static java.util.Collections.emptySet;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;

public class IndexerHelper {
    private static final Logger logger = LoggerFactory.getLogger(IndexerHelper.class);

    private static final String ELASTIC_SEARCH_URL = "http://localhost:9200";
    private static final String INDEXER_URL_FORMAT = "http://localhost:%d/rest/indexes";

    private final WebTarget indexerTarget;
    private final Client elasticSearchClient;
    private final ScheduledExecutorService executorService;

    public IndexerHelper(final javax.ws.rs.client.Client httpClient, final int indexerPort) {
        this.indexerTarget = httpClient.target(String.format(INDEXER_URL_FORMAT, indexerPort));
        this.elasticSearchClient = Client.builder()
                .serverUrl(ELASTIC_SEARCH_URL)
                .jsonRenderer(new GsonRenderer())
                .jsonParser(new GsonParser())
                .requestExecutor(OkHttpRequestExecutor.withCallFactory(new OkHttpClient.Builder().build()).build())
                .build();
        this.executorService = Executors.newSingleThreadScheduledExecutor();
    }

    public TestIndex createIndex(final RestIndexConfiguration indexConfiguration) throws IOException {
        final Response response = indexerTarget.request().post(Entity.json(indexConfiguration));

        assertThat(response.getStatus()).isEqualTo(Response.Status.ACCEPTED.getStatusCode());

        return new TestIndex(indexConfiguration);
    }

    public final class TestIndex {
        final String indexName;
        final String type;

        private TestIndex(final RestIndexConfiguration indexConfiguration) {
            this.indexName = indexConfiguration.getIndexName();
            this.type = indexConfiguration.getIndex()
                    .getMappings()
                    .entrySet()
                    .stream()
                    .map(Map.Entry::getKey)
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("No index mappings"));
        }

        public Collection<String> waitFor(
                final String queryAttribute,
                final String query,
                final String expectedAttribute,
                final Set<String> expectedAttributeValues) throws InterruptedException {
            final PollElasticSearch pollElasticSearch = new PollElasticSearch(
                    indexName, type, queryAttribute, query, expectedAttribute, expectedAttributeValues);
            final ScheduledFuture<?> scheduledFuture = executorService.scheduleAtFixedRate(pollElasticSearch, 0, 500, MILLISECONDS);

            try {
                pollElasticSearch.awaitUuidsFound(30, SECONDS);

                return pollElasticSearch.getValues();
            } finally {
                scheduledFuture.cancel(true);
            }
        }
    }

    private final class PollElasticSearch implements Runnable {
        private final String indexName;
        private final String type;
        private final String queryAttribute;
        private final String query;
        private final String expectedAttribute;
        private final Set<String> expectedAttributeValues;
        private final CountDownLatch found;

        private Set<String> values;

        private PollElasticSearch(
                final String indexName,
                final String type,
                final String queryAttribute,
                final String query,
                final String expectedAttribute,
                final Set<String> expectedAttributeValues) {
            this.indexName = indexName;
            this.type = type;
            this.queryAttribute = queryAttribute;
            this.query = query;
            this.expectedAttribute = expectedAttribute;
            this.expectedAttributeValues = expectedAttributeValues;
            this.found = new CountDownLatch(1);
            this.values = emptySet();
        }

        @Override
        public void run() {
            final SearchResponse searchResponse = elasticSearchClient.execute(queryBuilder()).join();
            values = extractValues(searchResponse);

            if (values.equals(expectedAttributeValues)) {
                logger.info("Values in query {}={} matches expectations: {}", queryAttribute, query, expectedAttributeValues);
                found.countDown();
            } else {
                logger.info("Values in query {}={} do not yet match expectations. Actual {}, Expected {}",
                        queryAttribute, query, values, expectedAttributeValues);
            }
        }

        private SearchRequestBuilder queryBuilder() {
            return index(indexName)
                    .type(type)
                    .search()
                    .source(searchSource()
                            .query(boolQuery()
                                    .must(matchPhraseQuery(queryAttribute)
                                            .query(query))));
        }

        private Set<String> extractValues(final SearchResponse searchResponse) {
            return searchResponse
                    .getHits()
                    .stream()
                    .map(Hit::getSource)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .map(content -> content.getStringOrThrow(expectedAttribute))
                    .collect(toSet());
        }

        boolean awaitUuidsFound(final long timeout, final TimeUnit unit) throws InterruptedException {
            return found.await(timeout, unit);
        }

        Collection<String> getValues() {
            return values;
        }
    }
}
