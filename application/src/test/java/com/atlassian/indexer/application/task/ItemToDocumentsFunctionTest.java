package com.atlassian.indexer.application.task;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.atlassian.indexer.application.entity.Document;
import com.atlassian.indexer.application.entity.NumberField;
import com.atlassian.indexer.application.entity.StringField;
import com.atlassian.indexer.model.index.DateField;
import com.atlassian.indexer.model.index.Index;
import com.atlassian.indexer.model.index.IndexField;
import com.atlassian.indexer.model.index.IndexMapping;
import com.atlassian.indexer.model.index.KeywordField;
import com.atlassian.indexer.model.index.LongField;
import com.atlassian.indexer.model.index.ObjectField;
import com.atlassian.indexer.model.index.TextField;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class ItemToDocumentsFunctionTest {
    @Test
    public void apply() throws Exception {
        final OffsetDateTime date = OffsetDateTime.now();
        final Map<String, AttributeValue> item = ImmutableMap.<String, AttributeValue>builder()
                        .put("uuid", new AttributeValue().withS("123A"))
                        .put("count", new AttributeValue().withN("3"))
                        .put("count2", new AttributeValue().withN("4"))
                        .put("date", new AttributeValue().withS(date.toString()))
                        .put("obj", new AttributeValue().withM(ImmutableMap.<String, AttributeValue>builder()
                                .put("numAsKeyword", new AttributeValue().withN("5"))
                                .build()))
                        .put("description", new AttributeValue().withS("some description"))
                        .build();

        final Index index = new Index(null, ImmutableMap.<String, IndexMapping>builder()
                .put("typeA", new IndexMapping(null, ImmutableMap.<String, IndexField>builder()
                        .put("uuid", new KeywordField())
                        .put("count", new LongField())
                        .put("date", new DateField())
                        .put("obj", new ObjectField(ImmutableMap.<String, IndexField>builder()
                                .put("numAsKeyword", new KeywordField())
                                .build()))
                        .put("description", new TextField())
                        .build()))
                .put("typeB", new IndexMapping(null, ImmutableMap.<String, IndexField>builder()
                        .put("uuid", new KeywordField())
                        .put("count2", new LongField())
                        .build()))
                .build());

        final ItemToDocumentsFunction itemToDocumentsFunction = ImmutableItemToDocumentsFunction.builder()
                .withIndex(index)
                .withDocumentIdAttribute("uuid")
                .build();

        final Set<Document> documents = itemToDocumentsFunction.apply(Document.Operation.PUT, item);

        final Set<Document> expectedDocuments = Sets.newHashSet(
                Document.builder("123A", "typeA", Document.Operation.PUT)
                        .addField(new StringField("uuid", "123A"))
                        .addField(new NumberField("count", new BigDecimal(3)))
                        .addField(new StringField("date", date.toString()))
                        .addField(com.atlassian.indexer.application.entity.ObjectField.builder("obj")
                                .addField(new NumberField("numAsKeyword", new BigDecimal(5)))
                                .build())
                        .addField(new StringField("description", "some description"))
                        .build(),
                Document.builder("123A", "typeB", Document.Operation.PUT)
                        .addField(new StringField("uuid", "123A"))
                        .addField(new NumberField("count2", new BigDecimal(4)))
                        .build());

        assertThat(documents).isEqualTo(expectedDocuments);
    }
}