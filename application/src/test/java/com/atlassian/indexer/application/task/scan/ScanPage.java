package com.atlassian.indexer.application.task.scan;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Page;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.model.ScanResult;

import java.util.List;

public final class ScanPage extends Page<Item, ScanOutcome> {
    public ScanPage(final List<Item> items) {
        super(items, new ScanOutcome(new ScanResult()));
    }

    @Override
    public boolean hasNextPage() {
        return false;
    }

    @Override
    public Page<Item, ScanOutcome> nextPage() {
        return null;
    }
}
