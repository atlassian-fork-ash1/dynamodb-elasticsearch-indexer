package com.atlassian.indexer.application.service;

import com.atlassian.indexer.application.entity.Document;
import com.atlassian.indexer.application.entity.ObjectField;
import com.atlassian.indexer.application.entity.StringField;
import com.atlassian.indexer.application.service.TestIndexServiceImpl.IndexedLatch;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class IndexedLatchTest {
    @Test
    public void matches() throws Exception {
        final Document document = Document.builder("id", "tyoe", Document.Operation.PUT)
                .addField(ObjectField.builder("content")
                        .addField(new StringField("name", "henry"))
                        .build())
                .build();

        final Map<String, String> fields = ImmutableMap.<String, String>builder()
                .put("content.name", "henry")
                .build();

        final IndexedLatch indexedLatch = new IndexedLatch(fields, 1);

        indexedLatch.matches(document);

        assertThat(indexedLatch.await(1, TimeUnit.MILLISECONDS)).isTrue();
    }
}