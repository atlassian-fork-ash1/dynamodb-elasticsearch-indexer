package com.atlassian.indexer.application.task.init;

import com.atlassian.indexer.application.command.CommandResult;
import com.atlassian.indexer.application.configuration.IndexConfiguration;
import com.atlassian.indexer.application.exception.IndexingException;
import com.atlassian.indexer.application.service.IndexService;
import com.atlassian.indexer.model.index.Index;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.indexer.application.ErrorKeys.ErrorKey.ELASTICSEARCH_INDEX_CREATION_FAILED;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InitialiseIndexTaskTest {
    @Mock
    private IndexService indexService;

    @Mock
    private IndexConfiguration indexConfiguration;

    @InjectMocks
    private InitialiseIndexTask initialiseIndexTask;

    @Test
    public void createIndex() throws Exception {
        final String indexName = "finger";
        final Index index = new Index(null, null);
        when(indexConfiguration.getIndexName()).thenReturn(indexName);
        when(indexConfiguration.getIndex()).thenReturn(index);

        when(indexService.createIndex(indexName, index)).thenReturn(CommandResult.ok());

        initialiseIndexTask.run();
    }

    @Test(expected = IndexingException.class)
    public void createIndexError() throws Exception {
        final String indexName = "finger";
        final Index index = new Index(null, null);
        when(indexConfiguration.getIndexName()).thenReturn(indexName);
        when(indexConfiguration.getIndex()).thenReturn(index);

        when(indexService.createIndex(indexName, index)).thenReturn(CommandResult.error(ELASTICSEARCH_INDEX_CREATION_FAILED, "error"));

        initialiseIndexTask.run();
    }
}