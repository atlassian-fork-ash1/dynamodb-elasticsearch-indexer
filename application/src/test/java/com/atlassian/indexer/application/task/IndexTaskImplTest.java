package com.atlassian.indexer.application.task;

import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class IndexTaskImplTest {
    @Test
    public void run() throws Exception {
        final IndexTask task1 = mock(IndexTask.class);
        final IndexTask task2 = mock(IndexTask.class);

        new IndexTaskImpl(Lists.newArrayList(task1, task2)).run();

        final InOrder inOrder = inOrder(task1, task2);

        inOrder.verify(task1).run();
        inOrder.verify(task2).run();
    }

    @Test
    public void shutdown() throws Exception {
        final IndexTask task1 = mock(IndexTask.class);
        final IndexTask task2 = mock(IndexTask.class);

        new IndexTaskImpl(Lists.newArrayList(task1, task2)).shutdown();

        final InOrder inOrder = inOrder(task1, task2);

        inOrder.verify(task1).shutdown();
        inOrder.verify(task2).shutdown();
    }
}