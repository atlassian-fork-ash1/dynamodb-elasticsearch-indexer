package com.atlassian.indexer.application.service;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.atlassian.indexer.application.configuration.IndexerConfiguration;
import com.atlassian.indexer.application.dao.LeaseDao;
import com.atlassian.indexer.application.entity.Lease;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.OffsetDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LeaseServiceImplTest {
    @Mock
    private LeaseDao leaseDao;

    @Mock
    private IndexerConfiguration config;

    @InjectMocks
    private LeaseServiceImpl leaseService;

    @Before
    public void setup() {
        when(config.getAcquireLeaseFrequencySeconds()).thenReturn(0L);
        when(config.getLeaseExpirySeconds()).thenReturn(5L);
    }

    @Test
    public void acquireLeaseNoExistingLease() throws Exception {
        final String leaseKey = "abc";

        when(leaseDao.get(leaseKey)).thenReturn(Optional.empty());
        when(leaseDao.put(any(Lease.class))).then(invocation -> {
            final Lease lease = invocation.getArgument(0);
            return Lease.builder(lease).setVersion(lease.getVersion() + 1).build();
        });

        final Lease acquiredLease = leaseService.acquireLease(leaseKey);

        assertThat(acquiredLease.getKey()).isEqualTo(leaseKey);
        assertThat(acquiredLease.getCheckpoint()).isNull();
        assertThat(acquiredLease.isComplete()).isFalse();
        assertThat(acquiredLease.isExpired()).isFalse();
        assertThat(acquiredLease.getVersion()).isEqualTo(1);
    }

    @Test
    public void acquireLeaseExistingLeaseComplete() throws Exception {
        final Lease existingLease = Lease.builder("abc")
                .setComplete(true)
                .build();

        when(leaseDao.get(existingLease.getKey())).thenReturn(Optional.of(existingLease));

        final Lease acquiredLease = leaseService.acquireLease(existingLease.getKey());

        assertThat(acquiredLease).isEqualTo(existingLease);
    }

    @Test
    public void acquireLeaseExistingLeaseExpired() throws Exception {
        final ImmutableMap<String, AttributeValue> checkpoint = ImmutableMap.<String, AttributeValue>builder()
                .put("a", new AttributeValue().withS("b"))
                .build();
        final Lease existingLease = Lease.builder("abc")
                .setComplete(false)
                .setCheckpoint(checkpoint)
                .setExpiry(OffsetDateTime.now().minusSeconds(1))
                .setVersion(1)
                .build();

        when(leaseDao.get(existingLease.getKey())).thenReturn(Optional.of(existingLease));
        when(leaseDao.put(any(Lease.class))).then(invocation -> {
            final Lease lease = invocation.getArgument(0);
            return Lease.builder(lease).setVersion(lease.getVersion() + 1).build();
        });

        final Lease acquiredLease = leaseService.acquireLease(existingLease.getKey());

        assertThat(acquiredLease.getKey()).isEqualTo(existingLease.getKey());
        assertThat(acquiredLease.getCheckpoint()).isEqualTo(checkpoint);
        assertThat(acquiredLease.isComplete()).isFalse();
        assertThat(acquiredLease.isExpired()).isFalse();
        assertThat(acquiredLease.getVersion()).isEqualTo(2);
    }

    @Test
    public void acquireLeaseExistingLeaseValid() throws Exception {
        final Lease existingLeaseValid = Lease.builder("abc")
                .setExpiry(OffsetDateTime.now().plusSeconds(1))
                .build();
        final Lease existingLeaseComplete = Lease.builder(existingLeaseValid)
                .setComplete(true)
                .build();

        when(leaseDao.get(existingLeaseValid.getKey()))
                .thenReturn(Optional.of(existingLeaseValid))
                .thenReturn(Optional.of(existingLeaseComplete));

        final Lease acquiredLease = leaseService.acquireLease(existingLeaseValid.getKey());

        assertThat(acquiredLease).isEqualTo(existingLeaseComplete);
    }

    @Test
    public void checkpoint() throws Exception {
        final Lease leaseToCheckpoint = Lease.builder("abc")
                .setComplete(false)
                .setExpiry(OffsetDateTime.now().minusSeconds(1))
                .setVersion(1)
                .build();
        final ImmutableMap<String, AttributeValue> checkpoint = ImmutableMap.<String, AttributeValue>builder()
                .put("a", new AttributeValue().withS("b"))
                .build();

        when(leaseDao.put(any(Lease.class))).then(invocation -> {
            final Lease lease = invocation.getArgument(0);
            return Lease.builder(lease).setVersion(lease.getVersion() + 1).build();
        });

        final Lease checkpointedLease = leaseService.checkpoint(leaseToCheckpoint, checkpoint);

        assertThat(checkpointedLease.getKey()).isEqualTo(leaseToCheckpoint.getKey());
        assertThat(checkpointedLease.getCheckpoint()).isEqualTo(checkpoint);
        assertThat(checkpointedLease.isComplete()).isFalse();
        assertThat(checkpointedLease.isExpired()).isFalse();
        assertThat(checkpointedLease.getVersion()).isEqualTo(2);
    }

    @Test
    public void checkpointLeaseLost() throws Exception {
        final Lease leaseToCheckpoint = Lease.builder("abc").build();
        final ImmutableMap<String, AttributeValue> checkpoint = ImmutableMap.<String, AttributeValue>builder()
                .put("a", new AttributeValue().withS("b"))
                .build();
        final Lease existingLease = Lease.builder("abc")
                .setComplete(true)
                .build();

        when(leaseDao.put(any(Lease.class))).thenThrow(ConditionalCheckFailedException.class);
        when(leaseDao.get(existingLease.getKey())).thenReturn(Optional.of(existingLease));

        final Lease reacquiredLease = leaseService.checkpoint(leaseToCheckpoint, checkpoint);

        assertThat(reacquiredLease).isEqualTo(existingLease);
    }
}