package com.atlassian.indexer.application.task.stream;

import com.amazonaws.services.dynamodbv2.streamsadapter.model.RecordAdapter;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorCheckpointer;
import com.amazonaws.services.kinesis.clientlibrary.types.InitializationInput;
import com.amazonaws.services.kinesis.clientlibrary.types.ProcessRecordsInput;
import com.amazonaws.services.kinesis.model.Record;
import com.atlassian.indexer.application.exception.ElasticSearchUnavailableException;
import com.atlassian.indexer.application.task.stream.command.CheckpointShardCommand;
import com.atlassian.indexer.application.task.stream.command.ProcessRecordsCommand;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class StreamProcessorTest {
    private static final String INDEX_NAME = "INDEX_NAME";

    private CheckpointShardCommand checkpointShardCommand;
    private InitializationInput initializationInput;
    private ProcessRecordsInput processRecordsInput;
    private ProcessRecordsCommand processRecordsCommand;
    private IRecordProcessorCheckpointer checkpointInput;
    private com.amazonaws.services.dynamodbv2.model.Record myInput;
    private RecordAdapter input;
    private StreamProcessor streamProcessor;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setup() {
        checkpointShardCommand = mock(CheckpointShardCommand.class);
        initializationInput = mock(InitializationInput.class);
        processRecordsInput = mock(ProcessRecordsInput.class);
        processRecordsCommand = mock(ProcessRecordsCommand.class);
        checkpointInput = mock(IRecordProcessorCheckpointer.class);
        myInput = mock(com.amazonaws.services.dynamodbv2.model.Record.class);
        input  = mock(RecordAdapter.class);

        streamProcessor = new StreamProcessor(checkpointShardCommand, processRecordsCommand, INDEX_NAME);
        streamProcessor.initialize(initializationInput);
    }

    @Test
    public final void processRecords() {
        final List<Record> records = Collections.nCopies(2, input);

        when(input.getInternalObject()).thenReturn(myInput);
        when(processRecordsInput.getRecords()).thenReturn(records);
        when(processRecordsInput.getCheckpointer()).thenReturn(checkpointInput);

        streamProcessor.processRecords(processRecordsInput);

        final ArgumentCaptor<ProcessRecordsCommand.ProcessRecordsCommandInput> captor = ArgumentCaptor
                .forClass(ProcessRecordsCommand.ProcessRecordsCommandInput.class);

        verify(processRecordsCommand).execute(captor.capture());
        assertThat(captor.getValue().getRecords()).containsExactly(myInput, myInput);
        verify(checkpointShardCommand).execute(checkpointInput);
    }

    @Test
    public final void processRecordsDoesNotMoveShardPointerIfElasticSearchUnavailableExceptionWasThrown() {
        doThrow(ElasticSearchUnavailableException.class).when(processRecordsCommand).execute(any());
        expectedException.expect(ElasticSearchUnavailableException.class);

        final StreamProcessor streamProcessor = new StreamProcessor(checkpointShardCommand, processRecordsCommand, INDEX_NAME);

        streamProcessor.processRecords(processRecordsInput);

        verifyNoMoreInteractions(checkpointShardCommand);
    }
}