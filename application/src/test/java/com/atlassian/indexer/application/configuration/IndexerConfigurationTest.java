package com.atlassian.indexer.application.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.configuration.ConfigurationException;
import io.dropwizard.configuration.ConfigurationFactory;
import io.dropwizard.configuration.ConfigurationFactoryFactory;
import io.dropwizard.configuration.DefaultConfigurationFactoryFactory;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.jersey.validation.Validators;
import org.junit.Test;

import javax.validation.ValidatorFactory;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class IndexerConfigurationTest {

    @Test
    public void testConfig() throws Exception {
        verifyConfiguration("config.yml");
    }

    @Test
    public void testConfigTest() throws Exception {
        verifyConfiguration("test.yml");
    }

    private void verifyConfiguration(final String configFileName) throws Exception {
        parseConfiguration(getPath(configFileName));
    }

    private File getPath(final String configFileName) throws URISyntaxException {
        try {
            return new File(getClass().getClassLoader().getResource(configFileName).toURI());
        } catch (final NullPointerException npe) {
            throw new IllegalArgumentException(String.format("Configuration file '%s' does not exist.", configFileName));
        }
    }

    private static IndexerConfiguration parseConfiguration(final File path)
            throws IOException, ConfigurationException {
        final ConfigurationFactoryFactory<IndexerConfiguration> configurationFactoryFactory =
                new DefaultConfigurationFactoryFactory<>();
        final ValidatorFactory validatorFactory = Validators.newConfiguration()
                .buildValidatorFactory();
        final ObjectMapper objectMapper = Jackson.newObjectMapper();
        final ConfigurationFactory<IndexerConfiguration> configurationFactory = configurationFactoryFactory.
                create(IndexerConfiguration.class, validatorFactory.getValidator(), objectMapper, "dw");

        return configurationFactory.build(path);
    }
}
