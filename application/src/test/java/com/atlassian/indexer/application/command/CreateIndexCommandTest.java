package com.atlassian.indexer.application.command;

import com.atlassian.elasticsearch.client.Client;
import com.atlassian.elasticsearch.client.indices.CreateIndexRequestBuilder;
import com.atlassian.elasticsearch.client.indices.CreateIndexResponse;
import com.atlassian.elasticsearch.client.indices.CreateIndexSourceBuilder;
import com.atlassian.elasticsearch.client.indices.FieldBuilder;
import com.atlassian.elasticsearch.client.indices.IndexExistsBuilder;
import com.atlassian.elasticsearch.client.indices.IndexExistsResponse;
import com.atlassian.elasticsearch.client.indices.MappingBuilder;
import com.atlassian.elasticsearch.client.indices.ObjectFieldBuilder;
import com.atlassian.indexer.model.index.AllSetting;
import com.atlassian.indexer.model.index.DateField;
import com.atlassian.indexer.model.index.Index;
import com.atlassian.indexer.model.index.IndexField;
import com.atlassian.indexer.model.index.IndexMapping;
import com.atlassian.indexer.model.index.IndexSettings;
import com.atlassian.indexer.model.index.KeywordField;
import com.atlassian.indexer.model.index.LongField;
import com.atlassian.indexer.model.index.ObjectField;
import com.atlassian.indexer.model.index.TextField;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static com.atlassian.elasticsearch.client.ES.createIndexSource;
import static com.atlassian.elasticsearch.client.ES.dateField;
import static com.atlassian.elasticsearch.client.ES.index;
import static com.atlassian.elasticsearch.client.ES.indexSettings;
import static com.atlassian.elasticsearch.client.ES.keywordField;
import static com.atlassian.elasticsearch.client.ES.longField;
import static com.atlassian.elasticsearch.client.ES.objectContent;
import static com.atlassian.elasticsearch.client.ES.objectField;
import static com.atlassian.elasticsearch.client.ES.textField;
import static com.atlassian.indexer.application.ErrorKeys.ErrorKey.ELASTICSEARCH_INDEX_CREATION_FAILED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CreateIndexCommandTest {
    private final Client searchClient = mock(Client.class);

    @Test
    public void testCreateIndex() {
        final Map<String, IndexField> subObjectProperties = ImmutableMap.of(
                "so1", new KeywordField()
        );

        final Map<String, IndexField> objectProperties = ImmutableMap.of(
                "o1", new KeywordField(),
                "o2", new LongField(),
                "o3", new DateField(),
                "o4", new ObjectField(subObjectProperties),
                "o5", new TextField()
        );

        final Map<String, IndexField> properties = ImmutableMap.of(
                "myKeyword", new KeywordField(),
                "myLong", new LongField(),
                "myDate", new DateField(),
                "myObject", new ObjectField(objectProperties),
                "myText", new TextField()
        );

        final String mappingType = "myMapping";

        final Map<String, IndexMapping> mappings = ImmutableMap.of(
                mappingType, new IndexMapping(new AllSetting(), properties)
        );

        final IndexSettings settings = new IndexSettings(5, 1, Integer.MAX_VALUE);
        final Index rawIndex = new Index(settings, mappings);

        final CreateIndexSourceBuilder source = createIndexSource()
                .settings(indexSettings()
                        .numberOfShards(settings.getNumberOfShards())
                        .numberOfReplicas(settings.getNumberOfReplicas())
                        .setting("max_result_window", settings.getMaxResultWindow().map(String::valueOf).get())
                        .setting("index.mapper.dynamic", "false"));


        final ObjectFieldBuilder subObjectField = objectField()
                .field("so1", keywordField());

        final FieldBuilder objectField = objectField()
                .field("o1", keywordField())
                .field("o2", longField())
                .field("o3", dateField())
                .field("o4", subObjectField)
                .field("o5", textField());

        final MappingBuilder mappingBuilder = new MappingBuilder()
                .field("myKeyword", keywordField())
                .field("myLong", longField())
                .field("myDate", dateField())
                .field("myObject", objectField)
                .field("myText", textField())
                .setting("_all", objectContent().with("enabled", true));

        source.mapping(mappingType, mappingBuilder);

        final String indexName = "pipelines";
        final IndexExistsBuilder exists = index(indexName).exists();

        final IndexExistsResponse indexExistsResponse = mock(IndexExistsResponse.class);
        final CompletableFuture<IndexExistsResponse> indexExistsResponseCompletable = CompletableFuture.completedFuture(indexExistsResponse);

        final CreateIndexResponse indexCreatedResponse = mock(CreateIndexResponse.class);
        final CompletableFuture<CreateIndexResponse> indexCreatedResponseCompletable = CompletableFuture.completedFuture(indexCreatedResponse);

        final CreateIndexRequestBuilder expectedIndex = new CreateIndexRequestBuilder(indexName).source(source);

        when(indexExistsResponse.getIndex()).thenReturn(indexName);
        when(indexExistsResponse.indexExists()).thenReturn(false);

        when(indexCreatedResponse.isStatusSuccess()).thenReturn(true);

        when(searchClient.execute(exists)).thenReturn(indexExistsResponseCompletable);
        when(searchClient.execute(expectedIndex)).thenReturn(indexCreatedResponseCompletable);

        final CommandResult<Void> commandResult = new CreateIndexCommand(searchClient, indexName, rawIndex).run();
        assertThat(commandResult.isError()).isFalse();
        verify(searchClient).execute(expectedIndex);
    }

    @Test
    public void testCreateIndexAlreadyExists() {
        final String indexName = "pipelines";
        final Map<String, IndexMapping> mappings = new HashMap<>();
        final IndexSettings settings = new IndexSettings(5, 1, Integer.MAX_VALUE);
        final Index rawIndex = new Index(settings, mappings);

        final IndexExistsBuilder exists = index(indexName).exists();
        final IndexExistsResponse indexExistsResponse = mock(IndexExistsResponse.class);
        final CompletableFuture<IndexExistsResponse> indexExistsResponseCompletable = CompletableFuture.completedFuture(indexExistsResponse);
        when(searchClient.execute(exists)).thenReturn(indexExistsResponseCompletable);
        when(indexExistsResponse.indexExists()).thenReturn(true);

        final CommandResult<Void> commandResult = new CreateIndexCommand(searchClient, indexName, rawIndex).run();

        assertThat(commandResult.isError()).isFalse();
    }

    @Test
    public void testCreateIndexAlreadyExistsOnCreate() {
        final IndexSettings settings = new IndexSettings(5, 1, Integer.MAX_VALUE);
        final Index rawIndex = new Index(settings, new HashMap<>());

        final CreateIndexSourceBuilder source = createIndexSource()
                .settings(indexSettings()
                        .numberOfShards(settings.getNumberOfShards())
                        .numberOfReplicas(settings.getNumberOfReplicas())
                        .setting("max_result_window", settings.getMaxResultWindow().map(String::valueOf).get())
                        .setting("index.mapper.dynamic", "false"));

        final String indexName = "pipelines";
        final IndexExistsBuilder exists = index(indexName).exists();

        final IndexExistsResponse indexExistsResponse = mock(IndexExistsResponse.class);
        final CompletableFuture<IndexExistsResponse> indexExistsResponseCompletable = CompletableFuture.completedFuture(indexExistsResponse);

        final CreateIndexResponse indexCreatedResponse = mock(CreateIndexResponse.class);
        final CompletableFuture<CreateIndexResponse> indexCreatedResponseCompletable = CompletableFuture.completedFuture(indexCreatedResponse);

        final CreateIndexRequestBuilder expectedIndex = new CreateIndexRequestBuilder(indexName).source(source);

        when(indexExistsResponse.getIndex()).thenReturn(indexName);
        when(indexExistsResponse.indexExists()).thenReturn(false);

        when(indexCreatedResponse.isStatusSuccess()).thenReturn(false);
        when(indexCreatedResponse.getContent()).thenReturn(objectContent().with("type", "index_already_exists_exception").build());

        when(searchClient.execute(exists)).thenReturn(indexExistsResponseCompletable);
        when(searchClient.execute(expectedIndex)).thenReturn(indexCreatedResponseCompletable);

        final CommandResult<Void> commandResult = new CreateIndexCommand(searchClient, indexName, rawIndex).run();
        assertThat(commandResult.isError()).isFalse();
        verify(searchClient).execute(expectedIndex);
    }

    @Test
    public void testCreateIndexError() {
        final IndexSettings settings = new IndexSettings(5, 1, Integer.MAX_VALUE);
        final Index rawIndex = new Index(settings, new HashMap<>());

        final CreateIndexSourceBuilder source = createIndexSource()
                .settings(indexSettings()
                        .numberOfShards(settings.getNumberOfShards())
                        .numberOfReplicas(settings.getNumberOfReplicas())
                        .setting("max_result_window", settings.getMaxResultWindow().map(String::valueOf).get())
                        .setting("index.mapper.dynamic", "false"));

        final String indexName = "pipelines";
        final IndexExistsBuilder exists = index(indexName).exists();

        final IndexExistsResponse indexExistsResponse = mock(IndexExistsResponse.class);
        final CompletableFuture<IndexExistsResponse> indexExistsResponseCompletable = CompletableFuture.completedFuture(indexExistsResponse);

        final CreateIndexResponse indexCreatedResponse = mock(CreateIndexResponse.class);
        final CompletableFuture<CreateIndexResponse> indexCreatedResponseCompletable = CompletableFuture.completedFuture(indexCreatedResponse);

        final CreateIndexRequestBuilder expectedIndex = new CreateIndexRequestBuilder(indexName).source(source);

        when(indexExistsResponse.getIndex()).thenReturn(indexName);
        when(indexExistsResponse.indexExists()).thenReturn(false);

        when(indexCreatedResponse.isStatusSuccess()).thenReturn(false);
        when(indexCreatedResponse.getContent()).thenReturn(objectContent().build());

        when(searchClient.execute(exists)).thenReturn(indexExistsResponseCompletable);
        when(searchClient.execute(expectedIndex)).thenReturn(indexCreatedResponseCompletable);

        final CommandResult<Void> commandResult = new CreateIndexCommand(searchClient, indexName, rawIndex).run();
        assertThat(commandResult.isError()).isTrue();
        assertThat(commandResult.getErrorKey()).isEqualTo(ELASTICSEARCH_INDEX_CREATION_FAILED);
        verify(searchClient).execute(expectedIndex);
    }
}