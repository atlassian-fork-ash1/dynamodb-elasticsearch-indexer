package com.atlassian.indexer.application.command;

import com.atlassian.elasticsearch.client.Client;
import com.atlassian.elasticsearch.client.indices.DeleteIndexRequestBuilder;
import com.atlassian.elasticsearch.client.indices.DeleteIndexResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.CompletableFuture;

import static com.atlassian.indexer.application.ErrorKeys.ErrorKey.ELASTICSEARCH_INDEX_DELETE_FAILED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DeleteIndexCommandTest {
    @Mock
    private Client searchClient;

    @Mock
    private CompletableFuture<DeleteIndexResponse> future;

    @Mock
    private DeleteIndexResponse response;

    @Before
    public void setup() {
        when(searchClient.execute(Mockito.<DeleteIndexRequestBuilder>any())).thenReturn(future);
        when(future.join()).thenReturn(response);
        when(response.isStatusSuccess()).thenReturn(true);
    }

    @Test
    public void testDeleteIndex() throws Exception {
        final String indexName = "fred";
        final DeleteIndexCommand deleteIndexCommand = new DeleteIndexCommand(searchClient, indexName);

        final CommandResult<Void> result = deleteIndexCommand.execute();

        assertThat(result.isError()).isFalse();
    }

    @Test
    public void testDeleteIndexNotFound() throws Exception {
        final String indexName = "fred";
        final DeleteIndexCommand deleteIndexCommand = new DeleteIndexCommand(searchClient, indexName);

        when(response.isStatusSuccess()).thenReturn(false);
        when(response.getStatusCode()).thenReturn(404);

        final CommandResult<Void> result = deleteIndexCommand.execute();

        assertThat(result.isError()).isFalse();
    }

    @Test
    public void testDeleteIndexError() throws Exception {
        final String indexName = "fred";
        final DeleteIndexCommand deleteIndexCommand = new DeleteIndexCommand(searchClient, indexName);

        when(response.isStatusSuccess()).thenReturn(false);
        when(response.getStatusCode()).thenReturn(500);

        final CommandResult<Void> result = deleteIndexCommand.execute();

        assertThat(result.isError()).isTrue();
        assertThat(result.getErrorKey()).isEqualTo(ELASTICSEARCH_INDEX_DELETE_FAILED);
    }
}