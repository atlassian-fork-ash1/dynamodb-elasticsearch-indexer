package com.atlassian.indexer.application.task.scan;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.atlassian.indexer.application.command.CommandResult;
import com.atlassian.indexer.application.configuration.IndexConfiguration;
import com.atlassian.indexer.application.dao.ItemDao;
import com.atlassian.indexer.application.entity.Document;
import com.atlassian.indexer.application.entity.Lease;
import com.atlassian.indexer.application.service.IndexService;
import com.atlassian.indexer.application.service.LeaseService;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static com.atlassian.indexer.application.ErrorKeys.ErrorKey.ELASTICSEARCH_INDEX_UPDATE_FAILED;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ScanIndexTaskTest {
    @Mock
    private ItemDao itemDao;

    @Mock
    private LeaseService leaseService;

    @Mock
    private IndexConfiguration indexConfiguration;

    @Mock
    private IndexService indexService;

    @Mock
    private ItemsToDocumentMapFunction itemsToDocumentsFunction;

    @InjectMocks
    private ScanIndexTask scanIndexTask;

    @Test
    public void run() throws Exception {
        final String indexName = "TestIndex";

        final Map<String, AttributeValue> item1 = ImmutableMap.<String, AttributeValue>builder().put("uuid", new AttributeValue().withS("1")).build();
        final Map<String, AttributeValue> item2 = ImmutableMap.<String, AttributeValue>builder().put("uuid", new AttributeValue().withS("2")).build();
        final Map<String, AttributeValue> item3 = ImmutableMap.<String, AttributeValue>builder().put("uuid", new AttributeValue().withS("3")).build();

        final Lease lease = Lease.builder(indexName).build();
        final Lease checkpointedLease = Lease.builder(indexName).setCheckpoint(item2).build();
        final Lease completedLease = Lease.builder(indexName).setComplete(true).build();

        final ScanResult page1 = new ScanResult().withItems(item1, item2).withCount(2).withLastEvaluatedKey(item2);
        final ScanResult page2 = new ScanResult().withItems(item3).withCount(1);

        final Map<String, Set<Document>> page1Documents = ImmutableMap.<String, Set<Document>>builder()
                .put("typeA", Sets.newHashSet(
                        Document.builder("1", "typeA", Document.Operation.PUT).build(),
                        Document.builder("2", "typeA", Document.Operation.PUT).build()))
                .build();
        final Map<String, Set<Document>> page2Documents = ImmutableMap.<String, Set<Document>>builder()
                .put("typeA", Sets.newHashSet(Document.builder("3", "typeA", Document.Operation.PUT).build()))
                .put("typeB", Sets.newHashSet(Document.builder("3", "typeB", Document.Operation.PUT).build()))
                .build();

        when(indexConfiguration.getIndexName()).thenReturn(indexName);
        when(leaseService.acquireLease(indexName)).thenReturn(lease).thenReturn(checkpointedLease).thenReturn(completedLease);
        when(itemDao.getItems(lease.getCheckpoint())).thenReturn(page1);
        when(itemDao.getItems(checkpointedLease.getCheckpoint())).thenReturn(page2);
        when(leaseService.checkpoint(lease, lease.getCheckpoint())).thenReturn(lease);
        when(leaseService.checkpoint(lease, page1.getLastEvaluatedKey())).thenReturn(checkpointedLease);
        when(leaseService.checkpoint(checkpointedLease, page2.getLastEvaluatedKey())).thenReturn(completedLease);
        when(itemsToDocumentsFunction.apply(page1.getItems())).thenReturn(page1Documents);
        when(itemsToDocumentsFunction.apply(page2.getItems())).thenReturn(page2Documents);
        when(indexService.index(indexName, page1Documents))
                .thenReturn(CommandResult.error(ELASTICSEARCH_INDEX_UPDATE_FAILED, "error"))
                .thenReturn(CommandResult.ok());
        when(indexService.index(indexName, page2Documents))
                .thenReturn(CommandResult.ok());

        scanIndexTask.run();

        verify(indexService, times(2)).index(indexName, page1Documents);
        verify(indexService).index(indexName, page2Documents);
    }

    @Test
    public void shutdown() throws Exception {
        final String indexName = "TestIndex";

        final Map<String, AttributeValue> item1 = ImmutableMap.<String, AttributeValue>builder().put("uuid", new AttributeValue().withS("1")).build();

        final Lease lease = Lease.builder(indexName).build();
        final Lease checkpointedLease = Lease.builder(indexName).setCheckpoint(item1).build();
        final Lease completedLease = Lease.builder(indexName).setComplete(true).build();

        final ScanResult page1 = new ScanResult().withItems(item1).withCount(1).withLastEvaluatedKey(item1);

        final Map<String, Set<Document>> page1Documents = ImmutableMap.<String, Set<Document>>builder()
                .put("typeA", Sets.newHashSet(Document.builder("1", "typeA", Document.Operation.PUT).build()))
                .build();
        final CountDownLatch waitUntilProcessingPage1 = new CountDownLatch(1);
        final CountDownLatch waitUntilShutdown = new CountDownLatch(1);

        when(indexConfiguration.getIndexName()).thenReturn(indexName);
        when(leaseService.acquireLease(indexName)).thenReturn(lease).thenReturn(checkpointedLease).thenReturn(completedLease);
        when(itemDao.getItems(lease.getCheckpoint())).thenReturn(page1);
        when(itemsToDocumentsFunction.apply(page1.getItems())).thenReturn(page1Documents);
        when(indexService.index(indexName, page1Documents))
                .then(invocation -> {
                    waitUntilProcessingPage1.countDown();
                    waitUntilShutdown.await(1, TimeUnit.SECONDS);
                    return CommandResult.ok();
                });

        new Thread(scanIndexTask).start();

        waitUntilProcessingPage1.await(1, TimeUnit.SECONDS);

        scanIndexTask.shutdown();

        waitUntilShutdown.countDown();

        verify(indexService).index(indexName, page1Documents);
        verifyNoMoreInteractions(indexService);
    }
}