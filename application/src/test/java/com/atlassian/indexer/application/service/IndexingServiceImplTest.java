package com.atlassian.indexer.application.service;

import com.atlassian.indexer.application.command.CommandResult;
import com.atlassian.indexer.application.command.DeleteIndexCommand;
import com.atlassian.indexer.application.command.SearchCommandFactory;
import com.atlassian.indexer.application.configuration.IndexConfiguration;
import com.atlassian.indexer.application.exception.IndexActiveException;
import com.atlassian.indexer.application.exception.IndexerException;
import com.atlassian.indexer.application.task.IndexTask;
import com.atlassian.indexer.application.task.IndexTaskFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static com.atlassian.indexer.application.ErrorKeys.ErrorKey.ELASTICSEARCH_INDEX_DELETE_FAILED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IndexingServiceImplTest {
    @Mock
    private SearchCommandFactory searchCommandFactory;

    @Mock
    private IndexTaskFactory indexTaskFactory;

    @InjectMocks
    private IndexingServiceImpl indexingService;

    @Test
    public void testCreateIndex() throws InterruptedException {
        final CountDownLatch countDownLatch = new CountDownLatch(1);

        final IndexTask indexTask = mock(IndexTask.class);
        final IndexConfiguration indexConfiguration = mock(IndexConfiguration.class);

        when(indexConfiguration.getIndexName()).thenReturn("Dex");
        when(indexTaskFactory.createIndexTask(indexConfiguration)).thenReturn(indexTask);
        doAnswer(invocation -> {
            countDownLatch.countDown();
            return null;
        }).when(indexTask).run();

        indexingService.createIndex(indexConfiguration);

        //Need both of the following statements to confirm that this was ONLY called once
        assertThat(countDownLatch.await(1L, TimeUnit.SECONDS))
                .withFailMessage("Index task was not run")
                .isTrue();
        verify(indexTask).run();
    }

    @Test
    public void stop() {
        final IndexTask indexTask = mock(IndexTask.class);
        final IndexConfiguration indexConfiguration = mock(IndexConfiguration.class);
        when(indexConfiguration.getIndexName()).thenReturn("Dex");
        when(indexTaskFactory.createIndexTask(indexConfiguration)).thenReturn(indexTask);

        indexingService.createIndex(indexConfiguration);

        indexingService.stop();
        verify(indexTask).shutdown();
    }

    @Test
    public void deleteIndex() throws Exception {
        final String indexName = "fred";
        final DeleteIndexCommand deleteIndexCommand = mock(DeleteIndexCommand.class);

        when(searchCommandFactory.deleteIndex(indexName)).thenReturn(deleteIndexCommand);
        when(deleteIndexCommand.execute()).thenReturn(CommandResult.ok());

        indexingService.deleteIndex(indexName);
    }

    @Test(expected = IndexActiveException.class)
    public void deleteActiveIndex() throws Exception {
        final CountDownLatch countDownLatch = new CountDownLatch(1);

        final String indexName = "fred";
        final IndexTask indexTask = mock(IndexTask.class);
        final IndexConfiguration indexConfiguration = mock(IndexConfiguration.class);

        when(indexConfiguration.getIndexName()).thenReturn(indexName);
        when(indexTaskFactory.createIndexTask(indexConfiguration)).thenReturn(indexTask);
        doAnswer(invocation -> {
            countDownLatch.countDown();
            return null;
        }).when(indexTask).run();

        indexingService.createIndex(indexConfiguration);

        assertThat(countDownLatch.await(1L, TimeUnit.SECONDS))
                .withFailMessage("Index task was not run")
                .isTrue();

        indexingService.deleteIndex(indexName);
    }

    @Test(expected = IndexerException.class)
    public void deleteIndexError() throws Exception {
        final String indexName = "fred";
        final DeleteIndexCommand deleteIndexCommand = mock(DeleteIndexCommand.class);

        when(searchCommandFactory.deleteIndex(indexName)).thenReturn(deleteIndexCommand);
        when(deleteIndexCommand.execute()).thenReturn(CommandResult.error(ELASTICSEARCH_INDEX_DELETE_FAILED, "error"));

        indexingService.deleteIndex(indexName);
    }
}