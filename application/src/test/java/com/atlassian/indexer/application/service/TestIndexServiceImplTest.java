package com.atlassian.indexer.application.service;

import com.atlassian.indexer.application.command.SearchCommandFactory;
import com.atlassian.indexer.application.entity.Document;
import com.atlassian.indexer.application.entity.StringField;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class TestIndexServiceImplTest {
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private SearchCommandFactory searchCommandFactory;

    @Test
    public void testAwaitIndexBefore() throws Exception {
        final TestIndexServiceImpl testIndexService = new TestIndexServiceImpl(searchCommandFactory);

        final String indexName = "test";
        final Map<String, String> fields = ImmutableMap.<String, String>builder().put("uuid", "A").build();

        final Map<String, Set<Document>> documentMap = ImmutableMap.<String, Set<Document>>builder()
                .put("type", ImmutableSet.<Document>builder()
                        .add(Document.builder("id", "type", Document.Operation.PUT)
                                .addField(new StringField("uuid", "A"))
                                .addField(new StringField("version", "1"))
                                .build())
                        .add(Document.builder("id", "type", Document.Operation.PUT)
                                .addField(new StringField("uuid", "A"))
                                .addField(new StringField("version", "2"))
                                .build())
                        .build())
                .build();

        testIndexService.index(indexName, documentMap);

        assertThat(testIndexService.await(indexName, fields, 2, 1, TimeUnit.SECONDS)).isTrue();
    }

    @Test
    public void testAwaitIndexAfter() throws Exception {
        final TestIndexServiceImpl testIndexService = new TestIndexServiceImpl(searchCommandFactory);

        final String indexName = "test";
        final Map<String, String> fields = ImmutableMap.<String, String>builder().put("uuid", "A").build();

        final Map<String, Set<Document>> documentMap = ImmutableMap.<String, Set<Document>>builder()
                .put("type", ImmutableSet.<Document>builder()
                        .add(Document.builder("id", "type", Document.Operation.PUT)
                                .addField(new StringField("uuid", "A"))
                                .addField(new StringField("version", "1"))
                                .build())
                        .add(Document.builder("id", "type", Document.Operation.PUT)
                                .addField(new StringField("uuid", "A"))
                                .addField(new StringField("version", "2"))
                                .build())
                        .build())
                .build();

        final Future<Boolean> await = Executors.newSingleThreadExecutor()
                .submit(() -> testIndexService.await(indexName, fields, 2, 1, TimeUnit.SECONDS));

        testIndexService.index(indexName, documentMap);

        assertThat(await.get()).isTrue();
    }
}