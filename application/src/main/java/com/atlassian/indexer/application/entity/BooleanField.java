package com.atlassian.indexer.application.entity;

public class BooleanField extends Field<Boolean> {
    public BooleanField(final String name, final Boolean value) {
        super(name, value);
    }
}
