package com.atlassian.indexer.application.service;

import com.atlassian.indexer.application.command.CommandResult;
import com.atlassian.indexer.application.command.SearchCommandFactory;
import com.atlassian.indexer.application.entity.Document;
import com.atlassian.indexer.model.index.Index;
import com.google.common.base.Joiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Implementation of {@link IndexService} that provides additional test utilities
 * such as the ability to wait for an item to be indexed.
 */
public class TestIndexServiceImpl implements IndexService {
    private static final Logger logger = LoggerFactory.getLogger(TestIndexServiceImpl.class);

    private final SearchCommandFactory searchCommandFactory;

    private final Map<String, Set<Document>> indexedDocuments;
    private final Map<String, Set<IndexedLatch>> latches;

    public TestIndexServiceImpl(final SearchCommandFactory searchCommandFactory) {
        this.searchCommandFactory = searchCommandFactory;
        this.indexedDocuments = new ConcurrentHashMap<>();
        this.latches = new ConcurrentHashMap<>();
    }

    @Override
    public CommandResult<Void> createIndex(final String indexName, final Index index) {
        return searchCommandFactory.createIndex(indexName, index).execute();
    }

    @Override
    public CommandResult<Void> index(final String indexName, final Map<String, Set<Document>> documentMap) {
        final boolean immediatelyRefreshIndex = true;
        final CommandResult<Void> result = searchCommandFactory.bulkIndex(indexName, immediatelyRefreshIndex, documentMap).execute();

        synchronized (this) {
            indexedDocuments.computeIfAbsent(indexName, s -> new HashSet<>())
                    .addAll(documentMap.values().stream().flatMap(Set::stream).collect(Collectors.toSet()));

            documentMap.forEach((type, documents) -> documents.forEach(document -> {
                latches.computeIfAbsent(indexName, s -> new HashSet<>()).forEach(indexedLatch -> indexedLatch.matches(document));
            }));
        }

        return result;
    }

    @Override
    public boolean await(final String indexName, final Map<String, String> fields, final int count, final long timeout, final TimeUnit unit)
            throws InterruptedException {
        final IndexedLatch indexedLatch = new IndexedLatch(fields, count);

        synchronized (this) {
            latches.computeIfAbsent(indexName, s -> new HashSet<>()).add(indexedLatch);

            indexedDocuments.computeIfAbsent(indexName, s -> new HashSet<>()).forEach(indexedLatch::matches);
        }

        final String fieldsString = Joiner.on(", ").withKeyValueSeparator(": ").join(fields);
        logger.debug("Index {}: Waiting on fields {}, count {}", indexName, fieldsString, count);

        final boolean await = indexedLatch.await(timeout, unit);

        logger.debug("Index {}: Wait on fields {}, count {} completed with response {}", indexName, fieldsString, count, await);

        return await;
    }

    public static class IndexedLatch {
        private final Map<String, String> fields;
        private final CountDownLatch latch;

        public IndexedLatch(final Map<String, String> fields, final int count) {
            this.fields = fields;
            this.latch = new CountDownLatch(count);
        }

        public void matches(final Document document) {
            final Set<Map.Entry<String, ?>> documentFields = document.getFields()
                    .stream()
                    .flatMap(field -> field.flatten(null))
                    .collect(Collectors.toSet());

            if (documentFields.containsAll(fields.entrySet())) {
                latch.countDown();
            }
        }

        public boolean await(final long timeout, final TimeUnit unit) throws InterruptedException {
            return latch.await(timeout, unit);
        }
    }
}
