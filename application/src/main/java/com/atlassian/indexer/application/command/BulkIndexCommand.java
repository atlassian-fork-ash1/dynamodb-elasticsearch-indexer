package com.atlassian.indexer.application.command;

import com.atlassian.elasticsearch.client.Client;
import com.atlassian.elasticsearch.client.content.ObjectContentBuilder;
import com.atlassian.elasticsearch.client.document.BulkDeleteActionBuilderNeedsId;
import com.atlassian.elasticsearch.client.document.BulkIndexActionBuilderNeedsSource;
import com.atlassian.elasticsearch.client.document.BulkRequestBuilder;
import com.atlassian.elasticsearch.client.document.BulkResponse;
import com.atlassian.indexer.application.configuration.tenacity.TenacityPropertyKeys;
import com.atlassian.indexer.application.entity.BooleanField;
import com.atlassian.indexer.application.entity.Document;
import com.atlassian.indexer.application.entity.Field;
import com.atlassian.indexer.application.entity.NumberField;
import com.atlassian.indexer.application.entity.ObjectField;
import com.atlassian.indexer.application.entity.StringField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;

import static com.atlassian.elasticsearch.client.ES.bulkDelete;
import static com.atlassian.elasticsearch.client.ES.bulkIndex;
import static com.atlassian.elasticsearch.client.ES.index;
import static com.atlassian.elasticsearch.client.ES.objectContent;
import static com.atlassian.indexer.application.ErrorKeys.ErrorKey.ELASTICSEARCH_INDEX_UPDATE_FAILED;

/**
 * A command to index items in elasticsearch.
 */
public class BulkIndexCommand extends AbstractSearchCommand<Void> {
    private static final Logger logger = LoggerFactory.getLogger(BulkIndexCommand.class);

    private final Client searchClient;
    private final String indexName;
    private final Map<String, Set<Document>> documentMap;
    private final boolean immediatelyRefreshIndex;

    BulkIndexCommand(final Client searchClient,
                     final String indexName,
                     final boolean immediatelyRefreshIndex,
                     final Map<String, Set<Document>> documentMap) {
        super(TenacityPropertyKeys.BULK_INDEX);

        this.searchClient = searchClient;
        this.indexName = indexName;
        this.immediatelyRefreshIndex = immediatelyRefreshIndex;
        this.documentMap = documentMap;
    }

    @Override
    protected CommandResult<Void> run() {
        final BulkRequestBuilder bulkRequest = index(indexName).bulk().refresh(immediatelyRefreshIndex);

        documentMap.forEach((type, documents) -> documents.forEach(document -> {
            logger.debug("Index {}: Indexing operation {} document {} version {}:{}",
                    indexName, document.getOperation(), document.getId(), document.getVersionType(), document.getVersion());

            switch (document.getOperation()) {
                case DELETE:
                    final BulkDeleteActionBuilderNeedsId bulkDelete = bulkDelete()
                            .type(type);
                    document.getVersion().ifPresent(bulkDelete::version);
                    document.getVersionType().ifPresent(bulkDelete::versionType);
                    bulkRequest.action(bulkDelete.id(document.getId()));
                    break;
                case PUT:
                    final BulkIndexActionBuilderNeedsSource bulkIndex = bulkIndex()
                            .type(type)
                            .id(document.getId());
                    document.getVersion().ifPresent(bulkIndex::version);
                    document.getVersionType().ifPresent(bulkIndex::versionType);
                    bulkRequest.action(bulkIndex.source(createObjectContent(document.getFields())));
                    break;
                default:
                    throw new IllegalStateException("Unsupported document operation " + document.getOperation());
            }
        }));

        final BulkResponse bulkIndexResponse = searchClient.execute(bulkRequest).join();

        if (!bulkIndexResponse.isStatusSuccess()) {
            return CommandResult.error(ELASTICSEARCH_INDEX_UPDATE_FAILED, "Could not update the index in elastic search");
        }

        bulkIndexResponse.getItems().stream()
                .filter(item -> !item.isStatusSuccess())
                .forEach(item -> logger.debug("Index {}: Item {} failed to index with status code {}", item.getIndex(), item.getId(), item.getStatusCode()));

        return CommandResult.ok();
    }

    private ObjectContentBuilder createObjectContent(final Set<Field<?>> fields) {
        final ObjectContentBuilder objectContent = objectContent();

        fields.forEach(field -> {
            if (field instanceof BooleanField) {
                objectContent.with(field.getName(), ((BooleanField) field).getValue());
            } else if (field instanceof StringField) {
                objectContent.with(field.getName(), ((StringField) field).getValue());
            } else if (field instanceof NumberField) {
                objectContent.with(field.getName(), ((NumberField) field).getValue());
            } else if (field instanceof ObjectField) {
                objectContent.with(field.getName(), createObjectContent(((ObjectField) field).getValue()));
            } else {
                throw new IllegalArgumentException("Unhandled field value " + field.getName());
            }
        });

        return objectContent;
    }
}
