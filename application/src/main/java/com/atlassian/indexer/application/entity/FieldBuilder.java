package com.atlassian.indexer.application.entity;

public interface FieldBuilder<T extends FieldBuilder> {
    T addField(Field field);
}
