package com.atlassian.indexer.application;

import com.atlassian.indexer.application.configuration.IndexerConfiguration;
import com.atlassian.indexer.application.configuration.tenacity.TenacityBundleConfigurationFactory;
import com.atlassian.indexer.application.resource.Resource;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.base.Strings;
import com.yammer.tenacity.core.bundle.TenacityBundleBuilder;
import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.configuration.UrlConfigurationSourceProvider;
import io.dropwizard.java8.Java8Bundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.ws.rs.ext.ExceptionMapper;

import static org.eclipse.jetty.http.HttpStatus.TOO_MANY_REQUESTS_429;

/**
 * Indexer {@link Application} for indexing DynamoDB tables in Elasticsearch.
 */
public class IndexerApplication extends Application<IndexerConfiguration> {
    private static final Logger logger = LoggerFactory.getLogger(IndexerApplication.class);

    private static final String DROPWIZARD_SERVER_ARGUMENT = "server";

    private AnnotationConfigApplicationContext applicationContext;

    public static void main(final String[] arguments) throws Exception {
        final String configFile = (arguments.length == 1) ? arguments[0] : null;

        if (Strings.isNullOrEmpty(configFile)) {
            throw new IllegalArgumentException("Invalid number of arguments provided. Should have been {config | test}.yml");
        }

        logger.debug("Starting service with configuration file {}", configFile);

        new IndexerApplication().run(DROPWIZARD_SERVER_ARGUMENT, ClassLoader.getSystemResource(configFile).toString());
    }

    @Override
    public void initialize(final Bootstrap<IndexerConfiguration> bootstrap) {
        applicationContext = new AnnotationConfigApplicationContext();

        bootstrap.getObjectMapper()
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL);

        bootstrap.setConfigurationSourceProvider(new SubstitutingSourceProvider(
                new UrlConfigurationSourceProvider(),
                new EnvironmentVariableSubstitutor(false)));

        bootstrap.addBundle(TenacityBundleBuilder.<IndexerConfiguration>newBuilder()
                .configurationFactory(new TenacityBundleConfigurationFactory())
                .mapAllHystrixRuntimeExceptionsTo(TOO_MANY_REQUESTS_429)
                .build());
        bootstrap.addBundle(new Java8Bundle());
    }

    @Override
    public void run(final IndexerConfiguration configuration, final Environment environment) throws Exception {
        applicationContext.register(ComponentConfiguration.class);
        applicationContext.getBeanFactory().registerSingleton(configuration.getClass().getName(), configuration);
        applicationContext.getBeanFactory().registerSingleton(environment.getClass().getName(), environment);

        applicationContext.refresh();

        applicationContext.getBeansWithAnnotation(Resource.class).values()
                .forEach(resource -> environment.jersey().register(resource));
        applicationContext.getBeansOfType(ExceptionMapper.class).values()
                .forEach(exceptionMapper -> environment.jersey().register(exceptionMapper));
    }
}
