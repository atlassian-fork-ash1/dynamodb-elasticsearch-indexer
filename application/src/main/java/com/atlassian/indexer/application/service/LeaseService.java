package com.atlassian.indexer.application.service;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.atlassian.indexer.application.entity.Lease;

import javax.annotation.Nullable;
import java.util.Map;

/**
 * Service for acquiring and checkpointing scan leases.
 */
public interface LeaseService {
    /**
     * Acquire the lease with the given key. A lease grants an instance permission to perform a scan
     * on the associated table and contains checkpoint information about where the scan is up to.
     *
     * This method blocks until the lease can be obtained. The lease will be granted if no other instance
     * holds the lease, if a held lease expires or when the scan has completed.
     *
     * @param leaseKey The key of the lease to acquire.
     * @return The lease.
     */
    Lease acquireLease(String leaseKey);

    /**
     * Checkpoints and extends the expiry time of the given lease. If the lease has been lost, this
     * method will block until the lease can be reacquired.
     *
     * @param lease The lease to checkpoint.
     * @param checkpoint The checkpoint value.
     * @return The checkpointed or reacquired lease.
     */
    Lease checkpoint(Lease lease, @Nullable Map<String, AttributeValue> checkpoint);
}
