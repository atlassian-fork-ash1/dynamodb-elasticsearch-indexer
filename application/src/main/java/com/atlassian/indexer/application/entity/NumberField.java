package com.atlassian.indexer.application.entity;

public class NumberField extends Field<Number> {
    public NumberField(final String name, final Number value) {
        super(name, value);
    }
}
