package com.atlassian.indexer.application.dao;

/**
 * Factory for creating {@link ItemDao}s.
 */
public interface ItemDaoFactory {
    /**
     * Create a new {@link ItemDao} using the provided configuration.
     * @param tableName The name of the table to index from.
     * @return a new ItemDao.
     */
    ItemDao createItemDao(String tableName);
}
