package com.atlassian.indexer.application.dao;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.atlassian.indexer.application.exception.TableNotFoundException;

import javax.annotation.Nullable;
import java.util.Map;

/**
 * DAO for interacting with DynamoDB {@link Item}s.
 */
public interface ItemDao {
    /**
     * @return the table description.
     * @throws TableNotFoundException if the table doesn't exist.
     */
    TableDescription getTableDescription();

    /**
     * @return the approximate item count from the table.
     */
    long countItems();

    /**
     * @param lastKeyEvaluated The starting key of the page to retrieve or null to retrieve the first page.
     * @return a page of items from the table starting from the last evaluated key provided.
     */
    ScanResult getItems(@Nullable Map<String, AttributeValue> lastKeyEvaluated);
}
