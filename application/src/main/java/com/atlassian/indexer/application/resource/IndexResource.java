package com.atlassian.indexer.application.resource;

import com.atlassian.indexer.application.configuration.IndexConfiguration;
import com.atlassian.indexer.application.service.IndexService;
import com.atlassian.indexer.application.service.IndexingService;
import com.atlassian.indexer.model.RestIndexConfiguration;
import com.atlassian.indexer.model.RestIndexWait;
import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.TimeUnit;

import static org.apache.http.HttpStatus.SC_ACCEPTED;
import static org.apache.http.HttpStatus.SC_FORBIDDEN;
import static org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR;
import static org.apache.http.HttpStatus.SC_OK;

/**
 * Index resource.
 */
@Api(value = "/rest/indexes", description = "Endpoints for interacting with indexes.")
@Path("/rest/indexes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Resource
public class IndexResource {
    private final IndexService indexService;
    private final IndexingService indexingService;

    @Autowired
    public IndexResource(final IndexService indexService, final IndexingService indexingService) {
        this.indexService = indexService;
        this.indexingService = indexingService;
    }

    @POST
    @ApiOperation("Create an index and start indexing.")
    @ApiResponses({
            @ApiResponse(code = SC_ACCEPTED, message = "The index was created."),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "An unexpected error occurred creating the index.")})
    @Timed
    public Response createIndex(
            @ApiParam(value = "The index to create.", required = true)
            final RestIndexConfiguration indexConfiguration) {
        indexingService.createIndex(IndexConfiguration.fromRest(indexConfiguration));

        return Response.accepted().build();
    }

    @POST
    @Path("{indexName}/await")
    @ApiOperation("Wait for documents to be indexed.")
    @ApiResponses({
            @ApiResponse(code = SC_OK, message = "The documents have been indexed or the wait timed out.")})
    @Timed
    public Response await(
            @ApiParam(value = "The name of the index to wait on.", required = true)
            @PathParam("indexName") final String indexName,
            @ApiParam(value = "The document conditions to wait for.", required = true)
            final RestIndexWait indexWait) throws InterruptedException {
        indexService.await(indexName, indexWait.getFields(), indexWait.getCount(), indexWait.getTimeoutMillis(), TimeUnit.MILLISECONDS);

        return Response.ok().build();
    }

    @DELETE
    @Path("{indexName}")
    @ApiOperation("Delete the index with the given name.")
    @ApiResponses({
            @ApiResponse(code = SC_OK, message = "The index was deleted."),
            @ApiResponse(code = SC_FORBIDDEN, message = "The index is still in use."),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "An unexpected error occurred deleting the index.")})
    @Timed
    public Response deleteIndex(
            @ApiParam(value = "The name of the index to delete.", required = true)
            @PathParam("indexName") final String indexName) {
        indexingService.deleteIndex(indexName);

        return Response.ok().build();
    }
}
