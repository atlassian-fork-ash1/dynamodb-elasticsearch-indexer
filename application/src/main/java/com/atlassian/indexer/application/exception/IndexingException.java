package com.atlassian.indexer.application.exception;

public class IndexingException extends IndexerException {
    public IndexingException(final String message) {
        super(message);
    }
}
