package com.atlassian.indexer.application.task.stream;

import com.amazonaws.services.dynamodbv2.model.Record;
import com.atlassian.indexer.application.entity.Document;
import com.atlassian.indexer.application.task.ItemToDocumentsFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RecordsToDocumentMapFunction implements Function<List<Record>, Map<String, Set<Document>>> {
    private static final Logger logger = LoggerFactory.getLogger(RecordsToDocumentMapFunction.class);

    private static final String INSERT_EVENT = "INSERT";
    private static final String MODIFY_EVENT = "MODIFY";
    private static final String REMOVE_EVENT = "REMOVE";

    private final ItemToDocumentsFunction itemToDocumentsFunction;

    public RecordsToDocumentMapFunction(final ItemToDocumentsFunction itemToDocumentsFunction) {
        this.itemToDocumentsFunction = itemToDocumentsFunction;
    }

    @Override
    public Map<String, Set<Document>> apply(final List<Record> records) {
        return records.stream().flatMap(record -> {
            final String eventName = record.getEventName().toUpperCase();

            if (eventName.equals(INSERT_EVENT) || eventName.equals(MODIFY_EVENT)) {
                return itemToDocumentsFunction.apply(Document.Operation.PUT, record.getDynamodb().getNewImage()).stream();
            }

            if (eventName.equals(REMOVE_EVENT)) {
                return itemToDocumentsFunction.apply(Document.Operation.DELETE, record.getDynamodb().getOldImage()).stream();
            }

            logger.debug("Unsupported event type {}, skipping record {}", record.getEventName(), record.getEventID());
            return Stream.empty();
        }).collect(Collectors.groupingBy(Document::getType, Collectors.toCollection(LinkedHashSet::new)));
    }
}
