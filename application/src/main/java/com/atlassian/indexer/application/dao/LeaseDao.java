package com.atlassian.indexer.application.dao;

import com.atlassian.indexer.application.entity.Lease;

import java.util.Optional;

/**
 * DAO for interacting with {@link Lease}s.
 */
public interface LeaseDao {
    Optional<Lease> get(String key);

    Lease put(Lease lease);
}
