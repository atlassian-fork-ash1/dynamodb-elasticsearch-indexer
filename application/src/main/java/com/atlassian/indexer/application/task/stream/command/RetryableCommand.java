package com.atlassian.indexer.application.task.stream.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class RetryableCommand<T> {
    private static final Logger logger = LoggerFactory.getLogger(RetryableCommand.class);

    protected final int numberOfAttempts;
    private final long backoffTimeInMillis;

    protected RetryableCommand(final int numberOfAttempts, final long backoffTimeInMillis) {
        this.numberOfAttempts = numberOfAttempts;
        this.backoffTimeInMillis = backoffTimeInMillis;
    }

    protected abstract void run(T input);

    protected void error(final T input, final int attempt, final Exception e) {
    }

    protected void fail(final T input) {
    }

    public void execute(final T input) {
        boolean executionSuccessful = false;

        for (int attempt = 0; attempt < numberOfAttempts; attempt++) {
            try {
                run(input);
                executionSuccessful = true;
                break;
            } catch (final Exception e) {
                error(input, attempt, e);
                backoff();
            }
        }

        if (!executionSuccessful) {
            fail(input);
        }
    }

    private void backoff() {
        try {
            Thread.sleep(backoffTimeInMillis);
        } catch (InterruptedException e) {
            logger.debug("Interrupted sleep", e);
        }
    }
}