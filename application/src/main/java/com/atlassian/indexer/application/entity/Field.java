package com.atlassian.indexer.application.entity;

import com.google.common.base.Joiner;

import javax.annotation.Nullable;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

public abstract class Field<T> {
    private final String name;
    private final T value;

    public Field(final String name, final T value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public T getValue() {
        return value;
    }

    public Stream<Map.Entry<String, ?>> flatten(@Nullable final String prefix) {
        return Stream.of(new SimpleImmutableEntry<String, Object>(Joiner.on('.').skipNulls().join(prefix, name), value));
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Field<?> field = (Field<?>) o;
        return Objects.equals(name, field.name) &&
                Objects.equals(value, field.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value);
    }

    @Override
    public String toString() {
        return "Field{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
