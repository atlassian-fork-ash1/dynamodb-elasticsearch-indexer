package com.atlassian.indexer.application.task.scan;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.atlassian.indexer.application.entity.Document;
import com.atlassian.indexer.application.task.ItemToDocumentsFunction;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ItemsToDocumentMapFunction implements Function<List<Map<String, AttributeValue>>, Map<String, Set<Document>>> {
    private final ItemToDocumentsFunction itemToDocumentsFunction;

    public ItemsToDocumentMapFunction(final ItemToDocumentsFunction itemToDocumentsFunction) {
        this.itemToDocumentsFunction = itemToDocumentsFunction;
    }

    @Override
    public Map<String, Set<Document>> apply(final List<Map<String, AttributeValue>> items) {
        return items.stream()
                .flatMap(item -> itemToDocumentsFunction.apply(Document.Operation.PUT, item).stream())
                .collect(Collectors.groupingBy(Document::getType, Collectors.toSet()));
    }
}
