package com.atlassian.indexer.application;

import com.google.common.base.Joiner;

public abstract class ErrorKeys {
    // Service key
    private static final String INDEXER = "indexer";

    // Error group keys
    private static final String ELASTICSEARCH = "elasticsearch";
    private static final String INDEX = "index";

    public enum ErrorKey {
        ELASTICSEARCH_FAILED_EXECUTION(ELASTICSEARCH, "failed-execution"),
        ELASTICSEARCH_UNAVAILABLE(ELASTICSEARCH, "unavailable"),
        ELASTICSEARCH_UNKNOWN(ELASTICSEARCH, "unknown-exception"),
        ELASTICSEARCH_INDEX_CREATION_FAILED(INDEX, "creation-failed"),
        ELASTICSEARCH_INDEX_DELETE_FAILED(INDEX, "delete-failed"),
        ELASTICSEARCH_INDEX_UPDATE_FAILED(INDEX, "update-failed");

        private final String key;

        ErrorKey(final String group, final String... parts) {
            this.key = Joiner.on('.').join(INDEXER, group, (Object[]) parts);
        }

        public String getKey() {
            return key;
        }
    }
}
