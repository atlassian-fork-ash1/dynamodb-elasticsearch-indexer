@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
package com.atlassian.indexer.application.dao;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;