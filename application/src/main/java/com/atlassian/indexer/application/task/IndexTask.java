package com.atlassian.indexer.application.task;

/**
 * A task that indexes DynamoDB table data in Elastic search.
 */
public interface IndexTask extends Runnable {
    /**
     * Shuts down any resources used by this index task.
     * Note this does NOT kill the index thread which must
     * be stopped independently by the caller.
     */
    void shutdown();
}
