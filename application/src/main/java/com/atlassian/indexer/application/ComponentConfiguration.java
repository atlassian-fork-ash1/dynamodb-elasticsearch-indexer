package com.atlassian.indexer.application;

import com.amazonaws.auth.AWS4Signer;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSCredentialsProviderChain;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.streamsadapter.AmazonDynamoDBStreamsAdapterClient;
import com.atlassian.aws.request.okhttp.AWSSigningRequestInterceptor;
import com.atlassian.elasticsearch.client.Client;
import com.atlassian.elasticsearch.client.gson.GsonParser;
import com.atlassian.elasticsearch.client.gson.GsonRenderer;
import com.atlassian.elasticsearch.client.okhttp.OkHttpRequestExecutor;
import com.atlassian.indexer.application.command.SearchCommandFactory;
import com.atlassian.indexer.application.configuration.IndexerConfiguration;
import com.atlassian.indexer.application.service.IndexService;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * Spring configuration class.
 */
@Configuration
@ComponentScan
public class ComponentConfiguration {
    @Bean
    public AmazonDynamoDB dynamoDBClient(
            final AWSCredentialsProvider awsCredentialsProvider,
            final IndexerConfiguration config) {
        final AmazonDynamoDBClientBuilder clientBuilder = AmazonDynamoDBClientBuilder.standard().withCredentials(awsCredentialsProvider);

        // When testing, the default AWS DynamoDB endpoint can be overridden to point at a local DynamoDB instance.
        if (config.getDynamoDbEndpoint().isPresent()) {
            clientBuilder.withEndpointConfiguration(new EndpointConfiguration(config.getDynamoDbEndpoint().get(), Regions.DEFAULT_REGION.getName()));
        } else {
            clientBuilder.withRegion(config.getAwsRegion());
        }

        return clientBuilder.build();
    }

    @Bean
    public DynamoDB dynamoDB(final AmazonDynamoDB client) {
        return new DynamoDB(client);
    }

    @Bean
    public AmazonDynamoDBStreamsAdapterClient dynamoDBStreamsAdapterClient(
            final AWSCredentialsProvider awsCredentialsProvider,
            final IndexerConfiguration config) {
        final AmazonDynamoDBStreamsAdapterClient adapterClient = new AmazonDynamoDBStreamsAdapterClient(awsCredentialsProvider);

        // When testing, the default AWS DynamoDB endpoint can be overridden to point at a local DynamoDB instance.
        if (config.getDynamoDbEndpoint().isPresent()) {
            adapterClient.setEndpoint(config.getDynamoDbEndpoint().get());
        } else {
            adapterClient.setRegion(Region.getRegion(config.getAwsRegion()));
        }

        return adapterClient;
    }

    @Bean
    public AWS4Signer aws4Signer(final IndexerConfiguration config) {
        final AWS4Signer signer = new AWS4Signer();
        signer.setServiceName("es");
        signer.setRegionName(config.getAwsRegion().getName());
        return signer;
    }

    @Bean
    public AWSCredentialsProvider awsCredentialsProvider() {
        return new AWSCredentialsProviderChain(
                new DefaultAWSCredentialsProviderChain(),
                new AWSStaticCredentialsProvider(new BasicAWSCredentials("key", "secret")));
    }

    @Bean
    public OkHttpClient okHttpClient(final AWS4Signer signer, final AWSCredentialsProvider awsCredentialsProvider) {
        return new OkHttpClient.Builder()
                .addNetworkInterceptor(new AWSSigningRequestInterceptor(signer, awsCredentialsProvider))
                .build();
    }

    @Bean
    public Client searchClient(final IndexerConfiguration config, final OkHttpClient okHttpClient) {
        return Client.builder()
                .serverUrl(config.getElasticSearchUrl())
                .jsonRenderer(new GsonRenderer())
                .jsonParser(new GsonParser())
                .requestExecutor(OkHttpRequestExecutor
                        .withCallFactory(okHttpClient)
                        .onCloseCallback(() -> {
                            // There's no close method on the client, see Javadoc of OkHttpClient
                            okHttpClient.dispatcher().executorService().shutdown();
                            okHttpClient.connectionPool().evictAll();
                            if (okHttpClient.cache() != null) {
                                try {
                                    okHttpClient.cache().close();
                                } catch (final IOException e) {
                                    // The implementation doesn't actually throw an exception, so don't propagate it in the signature.
                                    throw new RuntimeException("Error closing OkHttpClient cache", e);
                                }
                            }
                        })
                        .build())
                .build();
    }

    @Bean
    public IndexService indexService(final IndexerConfiguration config, final SearchCommandFactory searchCommandFactory) {
        try {
            return config.getIndexServiceClass()
                    .getDeclaredConstructor(SearchCommandFactory.class)
                    .newInstance(searchCommandFactory);
        } catch (ReflectiveOperationException e) {
            throw new IllegalStateException("Error instatiating IndexService class", e);
        }
    }
}
