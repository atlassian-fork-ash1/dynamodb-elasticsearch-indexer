package com.atlassian.indexer.application.command;

import com.atlassian.elasticsearch.client.Client;
import com.atlassian.indexer.application.entity.Document;
import com.atlassian.indexer.model.index.Index;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;

@Component
public class SearchCommandFactory {
    private final Client searchClient;

    @Autowired
    public SearchCommandFactory(final Client searchClient) {
        this.searchClient = searchClient;
    }

    public CreateIndexCommand createIndex(final String indexName, final Index index) {
        return new CreateIndexCommand(searchClient, indexName, index);
    }

    public BulkIndexCommand bulkIndex(final String indexName, final boolean immediatelyRefreshIndex, final Map<String, Set<Document>> documentMap) {
        return new BulkIndexCommand(searchClient, indexName, immediatelyRefreshIndex, documentMap);
    }

    public DeleteIndexCommand deleteIndex(final String indexName) {
        return new DeleteIndexCommand(searchClient, indexName);
    }
}
