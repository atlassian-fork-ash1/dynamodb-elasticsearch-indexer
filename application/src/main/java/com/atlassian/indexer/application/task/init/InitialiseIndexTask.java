package com.atlassian.indexer.application.task.init;

import com.atlassian.indexer.application.command.CommandResult;
import com.atlassian.indexer.application.configuration.IndexConfiguration;
import com.atlassian.indexer.application.exception.IndexingException;
import com.atlassian.indexer.application.service.IndexService;
import com.atlassian.indexer.application.task.IndexTask;
import com.atlassian.indexer.model.index.Index;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InitialiseIndexTask implements IndexTask {
    private static final Logger logger = LoggerFactory.getLogger(InitialiseIndexTask.class);

    private final IndexConfiguration indexConfiguration;
    private final IndexService indexService;

    public InitialiseIndexTask(final IndexConfiguration indexConfiguration, final IndexService indexService) {
        this.indexConfiguration = indexConfiguration;
        this.indexService = indexService;
    }

    @Override
    public void run() {
        final String indexName = indexConfiguration.getIndexName();
        logger.info("Index {}: Running InitialiseIndexTask", indexName);
        final Index index;
        try {
            index = indexConfiguration.getIndex();
        } catch (final Exception ex) {
            logger.debug("Index {}: Error retrieving index configuration", indexName, ex);
            throw ex;
        }

        logger.info("Index {}: Running InitialiseIndexTask", indexName);

        final CommandResult<Void> result = indexService.createIndex(indexName, index);

        if (result.isError()) {
            throw new IndexingException(String.format("Index %s: Error creating index, error: %s", indexName, result.getErrorKey()));
        }

        logger.info("Index {}: Created successfully", indexName);
    }

    @Override
    public void shutdown() {

    }
}
