package com.atlassian.indexer.application.service;

import com.atlassian.indexer.application.command.CommandResult;
import com.atlassian.indexer.application.entity.Document;
import com.atlassian.indexer.model.index.Index;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Class responsible for performing indexing operations.
 */
public interface IndexService {
    CommandResult<Void> createIndex(String indexName, Index index);

    CommandResult<Void> index(String indexName, Map<String, Set<Document>> documentMap);

    boolean await(String indexName, Map<String, String> fields, int count, long timeout, TimeUnit unit) throws InterruptedException;
}
