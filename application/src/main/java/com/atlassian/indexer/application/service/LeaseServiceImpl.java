package com.atlassian.indexer.application.service;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.atlassian.indexer.application.configuration.IndexerConfiguration;
import com.atlassian.indexer.application.dao.LeaseDao;
import com.atlassian.indexer.application.entity.Lease;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.time.OffsetDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

@Component
public final class LeaseServiceImpl implements LeaseService {
    private static final Logger logger = LoggerFactory.getLogger(LeaseServiceImpl.class);

    private final LeaseDao leaseDao;
    private final IndexerConfiguration config;
    private final ScheduledExecutorService executorService;

    @Autowired
    private LeaseServiceImpl(final LeaseDao leaseDao, final IndexerConfiguration config) {
        this.leaseDao = leaseDao;
        this.config = config;
        this.executorService = Executors.newSingleThreadScheduledExecutor();
    }

    @Override
    public Lease acquireLease(final String leaseKey) {
        Optional<Lease> lease = Optional.empty();

        final AcquireLeaseTask acquireLeaseTask = new AcquireLeaseTask(leaseKey);

        long taskDelay = 0;

        while (!lease.isPresent()) {
            logger.info("Attempting to acquire lease {}", leaseKey);

            final ScheduledFuture<Optional<Lease>> scheduledFuture = executorService.schedule(acquireLeaseTask, taskDelay, TimeUnit.SECONDS);

            try {
                lease = scheduledFuture.get();
            } catch (final InterruptedException | ExecutionException e) {
                logger.debug("Acquisition of lease {} interrupted, retrying...", leaseKey, e);
            }

            taskDelay = config.getAcquireLeaseFrequencySeconds();
        }

        logger.info("Lease {} acquired", leaseKey);

        return lease.get();
    }

    @Override
    public Lease checkpoint(final Lease lease, @Nullable final Map<String, AttributeValue> checkpoint) {
        final Lease checkpointedLease = Lease.builder(lease)
                .setCheckpoint(checkpoint)
                .setComplete(checkpoint == null)
                .setExpiry(OffsetDateTime.now().plusSeconds(config.getLeaseExpirySeconds()))
                .build();

        try {
            return leaseDao.put(checkpointedLease);
        } catch (ConditionalCheckFailedException e) {
            logger.info("Lease {} lost, attempting to reacquire...", lease.getKey());
            return acquireLease(lease.getKey());
        }
    }

    private final class AcquireLeaseTask implements Callable<Optional<Lease>> {
        private final String leaseKey;

        private AcquireLeaseTask(final String leaseKey) {
            this.leaseKey = leaseKey;
        }

        @Override
        public Optional<Lease> call() {
            final Optional<Lease> existingLease = leaseDao.get(leaseKey);

            if (existingLease.map(Lease::isComplete).orElse(false)) {
                return existingLease;
            }

            if (existingLease.map(Lease::isExpired).orElse(true)) {
                final Lease lease = existingLease.map(Lease::builder).orElse(Lease.builder(leaseKey))
                        .setExpiry(OffsetDateTime.now().plusSeconds(config.getLeaseExpirySeconds()))
                        .build();

                return Optional.of(leaseDao.put(lease));
            }

            return Optional.empty();
        }
    }
}
