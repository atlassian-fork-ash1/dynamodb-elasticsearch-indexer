package com.atlassian.indexer.application.configuration;

import com.atlassian.indexer.model.RestIndexConfiguration;
import com.atlassian.indexer.model.index.Index;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.Optional;

/**
 * Index configuration.
 */
@Value.Immutable
@Value.Style(init = "with*", get = {"is*", "get*"})
@JsonDeserialize(builder = ImmutableIndexConfiguration.Builder.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public interface IndexConfiguration {
    String getTableName();

    /**
     * @return the name of the DynamoDB Item attribute to use as the document id in the index.
     * If not specified the indexer will use the table range attribute or hash attribute if
     * there is no range.
     */
    Optional<String> getDocumentIdAttribute();

    String getStreamCheckpointTableName();

    String getIndexName();

    Index getIndex();

    /**
     * @return the name of the DynamoDB Item attribute to use as the version in the index.
     * If not specified elasticsearch will use internal versioning.
     *
     * @see <a href="https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-index_.html#index-versioning">Versioning</a>
     * @see <a href="https://www.elastic.co/blog/elasticsearch-versioning-support">Elasticsearch Versioning Support</a>
     */
    Optional<String> getVersionAttribute();

    Optional<String> getVersionType();

    static IndexConfiguration fromRest(final RestIndexConfiguration restIndexConfiguration) {
        return ImmutableIndexConfiguration.builder()
                .withTableName(restIndexConfiguration.getTableName())
                .withDocumentIdAttribute(restIndexConfiguration.getDocumentIdAttribute())
                .withStreamCheckpointTableName(restIndexConfiguration.getStreamCheckpointTableName())
                .withIndexName(restIndexConfiguration.getIndexName())
                .withIndex(restIndexConfiguration.getIndex())
                .withVersionAttribute(restIndexConfiguration.getVersionAttribute())
                .withVersionType(restIndexConfiguration.getVersionType())
                .build();
    }
}
