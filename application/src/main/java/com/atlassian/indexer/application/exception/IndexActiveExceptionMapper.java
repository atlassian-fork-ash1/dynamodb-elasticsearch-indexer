package com.atlassian.indexer.application.exception;

import org.springframework.stereotype.Component;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Exception mapper for {@link IndexActiveException}.
 */
@Component
public class IndexActiveExceptionMapper implements ExceptionMapper<IndexActiveException> {
    @Override
    public Response toResponse(final IndexActiveException exception) {
        return Response.status(Response.Status.FORBIDDEN)
            .type(MediaType.TEXT_PLAIN)
            .entity(exception.getMessage())
            .build();
    }
}
