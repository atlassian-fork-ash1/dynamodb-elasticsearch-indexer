package com.atlassian.indexer.application.task.stream;

import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import com.atlassian.indexer.application.task.IndexTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StreamIndexTask implements IndexTask {
    private static final Logger logger = LoggerFactory.getLogger(StreamIndexTask.class);

    private final Worker worker;

    public StreamIndexTask(final Worker worker) {
        this.worker = worker;
    }

    @Override
    public void run() {
        logger.info("Starting DynamoDB stream processor worker {}", worker.getApplicationName());
        worker.run();
    }

    @Override
    public void shutdown() {
        logger.info("Stopping DynamoDB stream processor worker {}", worker.getApplicationName());
        worker.shutdown();
    }
}
