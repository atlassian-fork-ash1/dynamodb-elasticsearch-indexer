package com.atlassian.indexer.application.command;

import com.yammer.tenacity.core.TenacityCommand;
import com.yammer.tenacity.core.properties.TenacityPropertyKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.indexer.application.ErrorKeys.ErrorKey.ELASTICSEARCH_FAILED_EXECUTION;
import static com.atlassian.indexer.application.ErrorKeys.ErrorKey.ELASTICSEARCH_UNAVAILABLE;
import static com.atlassian.indexer.application.ErrorKeys.ErrorKey.ELASTICSEARCH_UNKNOWN;

public abstract class AbstractSearchCommand<T> extends TenacityCommand<CommandResult<T>> {
    private static final Logger logger = LoggerFactory.getLogger(AbstractSearchCommand.class);

    AbstractSearchCommand(final TenacityPropertyKey tenacityPropertyKey) {
        super(tenacityPropertyKey);
    }

    @Override
    protected CommandResult<T> getFallback() {
        if (isFailedExecution()) {
            logger.debug("Execution failed returning fallback", getFailedExecutionException());
            return CommandResult.error(ELASTICSEARCH_FAILED_EXECUTION, getFailedExecutionException().getMessage());
        } else if (isResponseTimedOut()) {
            logger.debug("Execution timed out, returning fallback");
            return CommandResult.error(ELASTICSEARCH_UNAVAILABLE, "Timeout occurred when communicating with Elastic Search");
        }

        logger.debug("Error performing search returning fallback", getExecutionException());
        return CommandResult.error(ELASTICSEARCH_UNKNOWN, getExecutionException().getMessage());
    }
}
