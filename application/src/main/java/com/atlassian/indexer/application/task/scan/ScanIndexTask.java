package com.atlassian.indexer.application.task.scan;

import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.atlassian.indexer.application.command.CommandResult;
import com.atlassian.indexer.application.configuration.IndexConfiguration;
import com.atlassian.indexer.application.dao.ItemDao;
import com.atlassian.indexer.application.entity.Document;
import com.atlassian.indexer.application.entity.Lease;
import com.atlassian.indexer.application.service.IndexService;
import com.atlassian.indexer.application.service.LeaseService;
import com.atlassian.indexer.application.task.IndexTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class ScanIndexTask implements IndexTask {
    private static final Logger logger = LoggerFactory.getLogger(ScanIndexTask.class);

    private final ItemDao itemDao;
    private final LeaseService leaseService;
    private final IndexConfiguration indexConfiguration;
    private final IndexService indexService;
    private final ItemsToDocumentMapFunction itemsToDocumentsFunction;
    private final AtomicBoolean shutdown;

    public ScanIndexTask(final ItemDao itemDao,
                         final LeaseService leaseService,
                         final IndexConfiguration indexConfiguration,
                         final IndexService indexService,
                         final ItemsToDocumentMapFunction itemsToDocumentsFunction) {
        this.itemDao = itemDao;
        this.leaseService = leaseService;
        this.indexConfiguration = indexConfiguration;
        this.indexService = indexService;
        this.itemsToDocumentsFunction = itemsToDocumentsFunction;
        this.shutdown = new AtomicBoolean(false);
    }

    @Override
    public void run() {
        logger.info("Index {}: Starting scan...", indexConfiguration.getIndexName());

        Lease lease = leaseService.acquireLease(indexConfiguration.getIndexName());

        final long approxItemCount = itemDao.countItems();
        final AtomicInteger itemCount = new AtomicInteger(0);

        while (!lease.isComplete()) {
            if (shutdown.get()) {
                logger.info("Index {}: Scan task shutting down", indexConfiguration.getIndexName());
                break;
            }

            final ScanResult scanResult = itemDao.getItems(lease.getCheckpoint());

            logger.info("Index {}: Indexing {} items...", indexConfiguration.getIndexName(), scanResult.getCount());

            final String indexName = indexConfiguration.getIndexName();
            final Map<String, Set<Document>> documentMap = itemsToDocumentsFunction.apply(scanResult.getItems());
            final CommandResult<Void> result = indexService.index(indexName, documentMap);

            if (result.isError()) {
                logger.error("Index {}: Error indexing {} items from checkpoint {}, error: {}",
                        indexConfiguration.getIndexName(),
                        scanResult.getCount(),
                        lease.getCheckpoint(),
                        result.getErrorKey());
                lease = leaseService.checkpoint(lease, lease.getCheckpoint());
            } else {
                logger.debug("Index {}: Indexed {} of approximately {} items successfully",
                        indexConfiguration.getIndexName(),
                        itemCount.addAndGet(scanResult.getCount()),
                        approxItemCount);
                lease = leaseService.checkpoint(lease, scanResult.getLastEvaluatedKey());
            }
        }

        logger.info("Index {}: Scan complete", indexConfiguration.getIndexName());
    }

    @Override
    public void shutdown() {
        logger.info("Index {}: Shutdown called on scan task", indexConfiguration.getIndexName());
        shutdown.set(true);
    }
}
