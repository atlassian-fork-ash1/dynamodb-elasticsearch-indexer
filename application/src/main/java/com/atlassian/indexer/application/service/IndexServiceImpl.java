package com.atlassian.indexer.application.service;

import com.atlassian.indexer.application.command.CommandResult;
import com.atlassian.indexer.application.command.SearchCommandFactory;
import com.atlassian.indexer.application.entity.Document;
import com.atlassian.indexer.model.index.Index;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class IndexServiceImpl implements IndexService {
    private final SearchCommandFactory searchCommandFactory;

    public IndexServiceImpl(final SearchCommandFactory searchCommandFactory) {
        this.searchCommandFactory = searchCommandFactory;
    }

    @Override
    public CommandResult<Void> createIndex(final String indexName, final Index index) {
        return searchCommandFactory.createIndex(indexName, index).execute();
    }

    @Override
    public CommandResult<Void> index(final String indexName, final Map<String, Set<Document>> documentMap) {
        final boolean immediatelyRefreshIndex = false;
        return searchCommandFactory.bulkIndex(indexName, immediatelyRefreshIndex, documentMap).execute();
    }

    @Override
    public boolean await(final String indexName, final Map<String, String> fields, final int count, final long timeout, final TimeUnit unit)
            throws InterruptedException {
        throw new UnsupportedOperationException();
    }
}
