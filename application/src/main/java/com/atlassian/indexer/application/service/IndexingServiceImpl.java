package com.atlassian.indexer.application.service;

import com.atlassian.indexer.application.command.CommandResult;
import com.atlassian.indexer.application.command.SearchCommandFactory;
import com.atlassian.indexer.application.configuration.IndexConfiguration;
import com.atlassian.indexer.application.exception.IndexActiveException;
import com.atlassian.indexer.application.exception.IndexerException;
import com.atlassian.indexer.application.task.IndexTask;
import com.atlassian.indexer.application.task.IndexTaskFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Component
public final class IndexingServiceImpl implements IndexingService {
    private static final Logger logger = LoggerFactory.getLogger(IndexingServiceImpl.class);

    private final SearchCommandFactory searchCommandFactory;
    private final IndexTaskFactory indexTaskFactory;
    private final ExecutorService executorService;
    private final Map<String, IndexTask> tasks;

    @Autowired
    public IndexingServiceImpl(final SearchCommandFactory searchCommandFactory, final IndexTaskFactory indexTaskFactory) {
        this.searchCommandFactory = searchCommandFactory;
        this.indexTaskFactory = indexTaskFactory;
        this.executorService = Executors.newCachedThreadPool();

        this.tasks = new ConcurrentHashMap<>();
    }

    @Override
    public void start() {
        // Don't do anything on service start
    }

    @Override
    @SuppressWarnings("FutureReturnValueIgnored")
    public void createIndex(final IndexConfiguration indexConfiguration) {
        final IndexTask indexTask = indexTaskFactory.createIndexTask(indexConfiguration);

        final IndexTask existingIndexTask = tasks.putIfAbsent(indexConfiguration.getIndexName(), indexTask);

        if (existingIndexTask == null) {
            logger.info("Starting index {}...", indexConfiguration.getIndexName());

            executorService.execute(indexTask);
        } else {
            logger.info("Index {} already running.", indexConfiguration.getIndexName());
        }
    }

    @Override
    public void deleteIndex(final String indexName) {
        final boolean indexActive = tasks.containsKey(indexName);

        if (indexActive) {
            throw new IndexActiveException(String.format("Index %s is currently in use", indexName));
        }

        final CommandResult<Void> result = searchCommandFactory.deleteIndex(indexName).execute();

        if (result.isError()) {
            throw new IndexerException("Unexpected error deleting index, reason: " + result.getErrorMessage());
        }
    }

    @Override
    public void stop() {
        logger.debug("Stopping indexing...");

        executorService.shutdown();
        tasks.values().forEach(IndexTask::shutdown);
        awaitIndexTermination();

        logger.debug("Indexing stopped");
    }

    private void awaitIndexTermination() {
        try {
            final boolean terminated = executorService.awaitTermination(1, TimeUnit.MINUTES);

            if (!terminated) {
                logger.error("Index threads did not complete within await termination timeout");
            }
        } catch (final InterruptedException e) {
            logger.error("Index threads shutdown interrupted", e);
        }
    }
}
