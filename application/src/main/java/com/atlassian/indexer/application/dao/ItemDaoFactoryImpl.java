package com.atlassian.indexer.application.dao;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class ItemDaoFactoryImpl implements ItemDaoFactory {
    private final AmazonDynamoDB client;

    @Autowired
    public ItemDaoFactoryImpl(final AmazonDynamoDB client) {
        this.client = client;
    }

    @Override
    public ItemDao createItemDao(final String tableName) {
        return new ItemDaoImpl(client, tableName);
    }
}
