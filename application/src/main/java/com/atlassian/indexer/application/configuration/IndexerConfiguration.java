package com.atlassian.indexer.application.configuration;

import com.amazonaws.regions.Regions;
import com.atlassian.indexer.application.IndexerApplication;
import com.atlassian.indexer.application.service.IndexService;
import com.atlassian.indexer.application.service.IndexServiceImpl;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yammer.tenacity.core.config.TenacityConfiguration;
import io.dropwizard.Configuration;

import javax.validation.constraints.NotNull;
import java.util.Optional;

/**
 * {@link Configuration} for the {@link IndexerApplication}.
 *
 * In this class we want to use {@link NotNull} as it is a runtime annotation used by the yaml parser
 * to find where a required value is not present in the configuration file used.
 */
public class IndexerConfiguration extends Configuration {
    private static final int DEFAULT_LEASE_EXPIRY_SECONDS = 300;
    private static final int DEFAULT_ACQUIRE_LEASE_FREQUENCY_SECONDS = 30;

    @JsonProperty
    private int numberOfTimesToRetryRequests;

    @JsonProperty
    private int retryExponentialBackoffDelayInMilliseconds;

    @NotNull
    private String indexServiceClass = IndexServiceImpl.class.getName();

    @JsonProperty
    private String awsRegion;

    @JsonProperty
    private String dynamoDbEndpoint;

    @JsonProperty
    private String domain;

    @JsonProperty
    private String elasticSearchUrl;

    @NotNull
    @JsonProperty
    private String scanCheckpointTableName;

    @JsonProperty
    private long leaseExpirySeconds = DEFAULT_LEASE_EXPIRY_SECONDS;

    @JsonProperty
    private long acquireLeaseFrequencySeconds = DEFAULT_ACQUIRE_LEASE_FREQUENCY_SECONDS;

    @NotNull
    @JsonProperty
    private TenacityConfiguration elasticSearchTenacityConfig;

    public int getNumberOfTimesToRetryRequests() {
        return numberOfTimesToRetryRequests;
    }

    public int getRetryExponentialBackoffDelayInMilliseconds() {
        return retryExponentialBackoffDelayInMilliseconds;
    }

    public Class<? extends IndexService> getIndexServiceClass() {
        try {
            return (Class<? extends IndexService>) Class.forName(indexServiceClass);
        } catch (final ClassNotFoundException e) {
            throw new IllegalStateException("IndexService class not found");
        }
    }

    public Regions getAwsRegion() {
        return Regions.fromName(awsRegion);
    }

    public Optional<String> getDynamoDbEndpoint() {
        return Optional.ofNullable(dynamoDbEndpoint);
    }

    public String getDomain() {
        return domain;
    }

    public String getElasticSearchUrl() {
        return elasticSearchUrl;
    }

    public String getScanCheckpointTableName() {
        return scanCheckpointTableName;
    }

    public long getLeaseExpirySeconds() {
        return leaseExpirySeconds;
    }

    public long getAcquireLeaseFrequencySeconds() {
        return acquireLeaseFrequencySeconds;
    }

    public TenacityConfiguration getElasticSearchTenacityConfig() {
        return elasticSearchTenacityConfig;
    }
}
