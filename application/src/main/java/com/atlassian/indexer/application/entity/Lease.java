package com.atlassian.indexer.application.entity;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;

import javax.annotation.Nullable;
import java.time.OffsetDateTime;
import java.util.Map;
import java.util.Objects;

public final class Lease {
    private final String key;
    private final OffsetDateTime expiry;
    private final Map<String, AttributeValue> checkpoint;
    private final boolean complete;
    private final long version;

    private Lease(final Builder builder) {
        this.key = builder.key;
        this.expiry = builder.expiry;
        this.checkpoint = builder.checkpoint;
        this.complete = builder.complete;
        this.version = builder.version;
    }

    public String getKey() {
        return key;
    }

    public OffsetDateTime getExpiry() {
        return expiry;
    }

    public boolean isExpired() {
        return expiry.isBefore(OffsetDateTime.now());
    }

    @Nullable
    public Map<String, AttributeValue> getCheckpoint() {
        return checkpoint;
    }

    public boolean isComplete() {
        return complete;
    }

    public long getVersion() {
        return version;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Lease lease = (Lease) o;
        return complete == lease.complete &&
                version == lease.version &&
                Objects.equals(key, lease.key) &&
                Objects.equals(expiry, lease.expiry) &&
                Objects.equals(checkpoint, lease.checkpoint);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, expiry, checkpoint, complete, version);
    }

    public static Builder builder(final String key) {
        return new Builder(key);
    }

    public static Builder builder(final Lease scanCheckpoint) {
        return new Builder(scanCheckpoint);
    }

    public static final class Builder {
        private final String key;
        private OffsetDateTime expiry;
        private Map<String, AttributeValue> checkpoint;
        private boolean complete;
        private long version;

        private Builder(final String key) {
            this.key = key;
            this.expiry = OffsetDateTime.now();
            this.complete = false;
            this.version = 0;
        }

        private Builder(final Lease scanCheckpoint) {
            this.key = scanCheckpoint.key;
            this.expiry = scanCheckpoint.expiry;
            this.checkpoint = scanCheckpoint.checkpoint;
            this.complete = scanCheckpoint.complete;
            this.version = scanCheckpoint.version;
        }

        public Builder setExpiry(final OffsetDateTime expiry) {
            this.expiry = expiry;
            return this;
        }

        public Builder setCheckpoint(@Nullable final Map<String, AttributeValue> checkpoint) {
            this.checkpoint = checkpoint;
            return this;
        }

        public Builder setComplete(final boolean complete) {
            this.complete = complete;
            return this;
        }

        public Builder setVersion(final long version) {
            this.version = version;
            return this;
        }

        public Lease build() {
            return new Lease(this);
        }
    }
}
