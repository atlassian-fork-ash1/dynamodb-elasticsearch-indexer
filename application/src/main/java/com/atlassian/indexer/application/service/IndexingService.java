package com.atlassian.indexer.application.service;

import com.atlassian.indexer.application.configuration.IndexConfiguration;
import com.atlassian.indexer.application.exception.IndexActiveException;
import io.dropwizard.lifecycle.Managed;

/**
 * Service for indexing DynamoDB tables in elastic search.
 */
public interface IndexingService extends Managed {
    /**
     * Create and starting indexing the index with the given configuration.
     *
     * @param indexConfiguration The index configuration.
     */
    void createIndex(IndexConfiguration indexConfiguration);

    /**
     * Delete the index with the given name.
     *
     * @param indexName The name of the index to delete.
     * @throws IndexActiveException if the index is active.
     */
    void deleteIndex(String indexName) throws IndexActiveException;
}
