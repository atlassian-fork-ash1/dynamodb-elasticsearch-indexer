package com.atlassian.indexer.application.task.stream.command;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.Record;
import com.atlassian.indexer.application.command.CommandResult;
import com.atlassian.indexer.application.configuration.IndexConfiguration;
import com.atlassian.indexer.application.entity.Document;
import com.atlassian.indexer.application.exception.ElasticSearchUnavailableException;
import com.atlassian.indexer.application.exception.IndexingException;
import com.atlassian.indexer.application.service.IndexService;
import com.atlassian.indexer.application.task.stream.RecordsToDocumentMapFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.atlassian.indexer.application.ErrorKeys.ErrorKey.ELASTICSEARCH_UNAVAILABLE;

public class ProcessRecordsCommand extends RetryableCommand<ProcessRecordsCommand.ProcessRecordsCommandInput> {
    private static final Logger logger = LoggerFactory.getLogger(ProcessRecordsCommand.class);

    private static final int NUMBER_OF_ATTEMPTS = 10;
    private static final long BACKOFF_TIME_IN_MILLIS = TimeUnit.SECONDS.toMillis(3);

    private final IndexConfiguration indexConfiguration;
    private final IndexService indexService;
    private final RecordsToDocumentMapFunction recordsToDocumentMapFunction;

    public ProcessRecordsCommand(final IndexConfiguration indexConfiguration,
                                 final IndexService indexService,
                                 final RecordsToDocumentMapFunction recordsToDocumentMapFunction) {
        super(NUMBER_OF_ATTEMPTS, BACKOFF_TIME_IN_MILLIS);
        this.indexConfiguration = indexConfiguration;
        this.indexService = indexService;
        this.recordsToDocumentMapFunction = recordsToDocumentMapFunction;
    }

    @Override
    protected void run(final ProcessRecordsCommandInput processRecordsCommandInput) {
        final String indexName = indexConfiguration.getIndexName();
        logger.info("Index {}: Indexing {} records from shard {}", indexName, processRecordsCommandInput.getRecords().size(),
                processRecordsCommandInput.getShardId());

        final Map<String, Set<Document>> documentMap = recordsToDocumentMapFunction.apply(processRecordsCommandInput.getRecords());
        final CommandResult<Void> result = indexService.index(indexName, documentMap);

        if (result.isError()) {
            if (result.getErrorKey() == ELASTICSEARCH_UNAVAILABLE) {
                throw new ElasticSearchUnavailableException(result.getErrorMessage());
            } else {
                throw new IndexingException(String.format("Index %s: Error indexing %s records in shard %s, error: %s", indexName,
                        processRecordsCommandInput.getRecords().size(), processRecordsCommandInput.getShardId(), result.getErrorKey()));
            }
        } else {
            processRecordsCommandInput.getRecords().forEach(item -> {
                final Optional<AttributeValue> attributeValue = getUuid(item);
                final String uuid = attributeValue.map(AttributeValue::toString).orElse("no-uuid");
                logger.info("Found on event {} uuid {}", indexName, uuid);
            });
        }

        logger.debug("Index {}: Successfully indexed {} records in shard {}", indexName, processRecordsCommandInput.getRecords().size(),
                processRecordsCommandInput.getShardId());
    }

    @Override
    protected void error(final ProcessRecordsCommandInput processRecordsCommandInput, final int attempt, final Exception e) {
        logger.warn("Index {}: Error indexing {} records in shard {} on attempt {} of {}, cause: {}", indexConfiguration.getIndexName(),
                processRecordsCommandInput.getRecords().size(), processRecordsCommandInput.getShardId(), attempt, numberOfAttempts, e.getMessage(), e);
    }

    @Override
    protected void fail(final ProcessRecordsCommandInput processRecordsCommandInput) {
        logger.error("Index {}: Failed indexing {} records in shard {} after {} attempts.", indexConfiguration.getIndexName(),
                processRecordsCommandInput.getRecords().size(), processRecordsCommandInput.getShardId(), numberOfAttempts);
    }

    private Optional<AttributeValue> getUuid(final Record record) {
        if (record.getEventName().equalsIgnoreCase("INSERT") || record.getEventName().equalsIgnoreCase("MODIFY")) {
            return Optional.ofNullable((record.getDynamodb().getNewImage().get("uuid")));
        } else if (record.getEventName().equalsIgnoreCase("REMOVE")) {
            return Optional.ofNullable((record.getDynamodb().getOldImage().get("uuid")));
        } else {
            return Optional.empty();
        }
    }

    public static class ProcessRecordsCommandInput {
        private final String shardId;
        private final List<Record> records;

        public ProcessRecordsCommandInput(final String shardId, final List<Record> records) {
            this.shardId = shardId;
            this.records = records;
        }

        public String getShardId() {
            return shardId;
        }

        public List<Record> getRecords() {
            return records;
        }
    }
}
