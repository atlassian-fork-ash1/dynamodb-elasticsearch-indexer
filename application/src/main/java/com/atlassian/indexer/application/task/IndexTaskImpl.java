package com.atlassian.indexer.application.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class IndexTaskImpl implements IndexTask {
    private static final Logger logger = LoggerFactory.getLogger(IndexTaskImpl.class);

    private final List<IndexTask> tasks;

    public IndexTaskImpl(final List<IndexTask> tasks) {
        this.tasks = tasks;
    }

    @Override
    public void run() {
        logger.info("Running IndexTask");
        tasks.forEach(Runnable::run);
    }

    @Override
    public void shutdown() {
        tasks.forEach(IndexTask::shutdown);
    }
}
