package com.atlassian.indexer.application.configuration.tenacity;

import com.yammer.tenacity.core.TenacityCommand;
import com.yammer.tenacity.core.properties.TenacityPropertyKey;

/**
 * Collection of {@link TenacityPropertyKey}s used to configure the {@link TenacityCommand}s.
 */
public enum TenacityPropertyKeys implements TenacityPropertyKey {
    BULK_INDEX,
    CREATE_INDEX
}
