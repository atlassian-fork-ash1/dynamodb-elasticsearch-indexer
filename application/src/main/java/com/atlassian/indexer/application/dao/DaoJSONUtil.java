package com.atlassian.indexer.application.dao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.RuntimeJsonMappingException;
import io.dropwizard.jackson.Jackson;

import java.io.IOException;

public final class DaoJSONUtil {
    private static final ObjectMapper JSON = Jackson.newObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);

    private DaoJSONUtil() {
    }

    /**
     * @param value the object to be converted to JSON
     * @return the JSON representation of the object provided
     */
    public static String writeValueAsString(final Object value) {
        try {
            return JSON.writeValueAsString(value);
        } catch (final JsonProcessingException e) {
            final RuntimeJsonMappingException re = new RuntimeJsonMappingException("Could not serialize object to string.");
            re.initCause(e);
            throw re;
        }
    }

    /**
     * @param json the JSON string representation of the object
     * @param clazz the class of the object to be returned
     * @param <T> the type of the object to be returned
     * @return the object representation of the JSON string
     */
    public static <T> T readValue(final String json, final Class<T> clazz) {
        try {
            return JSON.readValue(json, clazz);
        } catch (final IOException e) {
            final RuntimeJsonMappingException re = new RuntimeJsonMappingException("Could not deserialize string to object.");
            re.initCause(e);
            throw re;
        }
    }

    /**
     * @param json the JSON string representation of the object
     * @param valueTypeRef the type reference of the object to be returned
     * @param <T> the type of the object to be returned
     * @return the object representation of the JSON string
     */
    public static <T> T readValue(final String json, final TypeReference<T> valueTypeRef) {
        try {
            return JSON.readValue(json, valueTypeRef);
        } catch (final IOException e) {
            final RuntimeJsonMappingException re = new RuntimeJsonMappingException("Could not deserialize string to object.");
            re.initCause(e);
            throw re;
        }
    }
}
