#!/bin/bash
set -e
set -x

git remote set-url origin ${BITBUCKET_GIT_SSH_ORIGIN}
git config --global user.email "pipelines-ab-bot@atlassian.com"
git config --global user.name "Pipelines-ab-bot"

docker login -u=$DOCKERHUB_USERNAME -p=$DOCKERHUB_PASSWORD

# Prepare and perform the maven release
mvn -e -B -V -nsu release:prepare -DscmCommentPrefix="[skip ci]" -DautoVersionSubmodules=true

# Release the artifact to nexus
mvn -e -B -V -nsu release:perform -DlocalCheckout=true -DskipTests -Darguments=-DskipTests
