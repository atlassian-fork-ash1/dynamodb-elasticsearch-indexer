package com.atlassian.indexer.model;

import com.atlassian.indexer.model.index.Index;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.Optional;

/**
 * Index configuration model.
 */
@Value.Immutable
@Value.Style(init = "with*", get = {"is*", "get*"})
@JsonDeserialize(builder = ImmutableRestIndexConfiguration.Builder.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public interface RestIndexConfiguration {
    String getTableName();

    Optional<String> getDocumentIdAttribute();

    String getStreamCheckpointTableName();

    String getIndexName();

    Index getIndex();

    Optional<String> getVersionAttribute();

    Optional<String> getVersionType();
}
