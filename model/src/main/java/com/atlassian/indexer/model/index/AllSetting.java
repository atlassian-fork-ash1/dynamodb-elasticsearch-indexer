package com.atlassian.indexer.model.index;

public class AllSetting {
    private boolean enabled = true;

    public boolean isEnabled() {
        return enabled;
    }
}
