package com.atlassian.indexer.model.index;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class IndexMapping implements IndexProperties {
    private final AllSetting allSetting;
    private final Map<String, IndexField> properties;

    @JsonCreator
    public IndexMapping(@JsonProperty("_all") @Nullable final AllSetting allSetting,
                        @JsonProperty("properties") @Nullable final Map<String, IndexField> properties) {
        this.allSetting = allSetting;
        this.properties = (properties != null) ? properties : new HashMap<>();
    }

    public Optional<AllSetting> getAllSetting() {
        return Optional.ofNullable(allSetting);
    }

    @Override
    public Map<String, IndexField> getProperties() {
        return properties;
    }
}
