package com.atlassian.indexer.model.index;

import java.util.Map;

/**
 * The properties associated with an elasticsearch index.
 */
public interface IndexProperties {
    Map<String, IndexField> getProperties();
}
