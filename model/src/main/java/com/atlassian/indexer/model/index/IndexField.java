package com.atlassian.indexer.model.index;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * A field on an elasticsearch index.
 * For a full list, see https://www.elastic.co/guide/en/elasticsearch/reference/6.3/mapping.html
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", defaultImpl = ObjectField.class)
@JsonSubTypes({
        @JsonSubTypes.Type(value = TextField.class, name = "text"),
        @JsonSubTypes.Type(value = KeywordField.class, name = "keyword"),
        @JsonSubTypes.Type(value = DateField.class, name = "date"),
        @JsonSubTypes.Type(value = LongField.class, name = "long"),
        @JsonSubTypes.Type(value = IntegerField.class, name = "integer"),
        @JsonSubTypes.Type(value = BooleanField.class, name = "boolean")
})
public interface IndexField {
}
